/* Generated by Nim Compiler v2.0.8 */
#define NIM_INTBITS 64

#include "nimbase.h"
#include <string.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct tyObject_TNode__dCqWNhWIK3POA9avZBdHukA tyObject_TNode__dCqWNhWIK3POA9avZBdHukA;
typedef struct tyObject_TType__hmsWK64eLkzu9axqdsyOa1A tyObject_TType__hmsWK64eLkzu9axqdsyOa1A;
typedef struct tyObject_TLineInfo__7niHWPAAHkAr9aD5TBIThXg tyObject_TLineInfo__7niHWPAAHkAr9aD5TBIThXg;
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ;
typedef struct tyObject_TIdent__Z9aK6f9cNu2HfiP40JgRCmBw tyObject_TIdent__Z9aK6f9cNu2HfiP40JgRCmBw;
typedef struct tySequence__Dna46DaMJFlkp8AvyILKcw tySequence__Dna46DaMJFlkp8AvyILKcw;
typedef struct tySequence__Dna46DaMJFlkp8AvyILKcw_Content tySequence__Dna46DaMJFlkp8AvyILKcw_Content;
typedef struct tyObject_TIdObj__nIRlm6AIb3flFb9aXcKZjDw tyObject_TIdObj__nIRlm6AIb3flFb9aXcKZjDw;
typedef struct RootObj RootObj;
typedef struct TNimTypeV2 TNimTypeV2;
typedef struct tyObject_ItemId__JbzKN1Pgny59c5hNg1tvLBA tyObject_ItemId__JbzKN1Pgny59c5hNg1tvLBA;
typedef struct tyObject_TLoc__xNf0VRJbHsSBgi4wLrInhw tyObject_TLoc__xNf0VRJbHsSBgi4wLrInhw;
typedef struct tyObject_TLib__wawv89bWj8toKwRoAU9c2PWA tyObject_TLib__wawv89bWj8toKwRoAU9c2PWA;
typedef struct tyObject_TNodeTable__w55cv29aPQUsY3ezLC6SPLg tyObject_TNodeTable__w55cv29aPQUsY3ezLC6SPLg;
typedef struct tySequence__Rb6lnVSG9cMda7VS4gHgKNQ tySequence__Rb6lnVSG9cMda7VS4gHgKNQ;
typedef struct tySequence__Rb6lnVSG9cMda7VS4gHgKNQ_Content tySequence__Rb6lnVSG9cMda7VS4gHgKNQ_Content;
typedef struct tyObject_TNodePair__0dsX107HfAY9bNJCptoRzaQ tyObject_TNodePair__0dsX107HfAY9bNJCptoRzaQ;
struct tyObject_TLineInfo__7niHWPAAHkAr9aD5TBIThXg {
	NU16 line;
	NI16 col;
	NI32 fileIndex;
};
typedef NU32 tySet_tyEnum_TNodeFlag__TQz5bJHIUpAWyJTrllZudg;
typedef NU8 tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw;
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
struct tySequence__Dna46DaMJFlkp8AvyILKcw {
  NI len; tySequence__Dna46DaMJFlkp8AvyILKcw_Content* p;
};
struct tyObject_TNode__dCqWNhWIK3POA9avZBdHukA {
	tyObject_TType__hmsWK64eLkzu9axqdsyOa1A* typ;
	tyObject_TLineInfo__7niHWPAAHkAr9aD5TBIThXg info;
	tySet_tyEnum_TNodeFlag__TQz5bJHIUpAWyJTrllZudg flags;
	tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw kind;
union{
struct {	NI64 intVal;
} _kind_1;
struct {	NF floatVal;
} _kind_2;
struct {	NimStringV2 strVal;
} _kind_3;
struct {	tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ* sym;
} _kind_4;
struct {	tyObject_TIdent__Z9aK6f9cNu2HfiP40JgRCmBw* ident;
} _kind_5;
struct {	tySequence__Dna46DaMJFlkp8AvyILKcw sons;
} _kind_6;
};
};
struct tyObject_TIdent__Z9aK6f9cNu2HfiP40JgRCmBw {
	NI id;
	NimStringV2 s;
	tyObject_TIdent__Z9aK6f9cNu2HfiP40JgRCmBw* next;
	NI h;
};
struct TNimTypeV2 {
	void* destructor;
	NI size;
	NI16 align;
	NI16 depth;
	NU32* display;
	void* traceImpl;
	void* typeInfoV1;
	NI flags;
};
struct RootObj {
TNimTypeV2* m_type;
};
struct tyObject_ItemId__JbzKN1Pgny59c5hNg1tvLBA {
	NI32 module;
	NI32 item;
};
struct tyObject_TIdObj__nIRlm6AIb3flFb9aXcKZjDw {
  RootObj Sup;
	tyObject_ItemId__JbzKN1Pgny59c5hNg1tvLBA itemId;
};
typedef NU8 tyEnum_TSymKind__N5y2omXzpRTZByIbdpos3g;
typedef NU16 tyEnum_TMagic__VHGY7C2GQZGrk80jDSEpVw;
typedef NU64 tySet_tyEnum_TSymFlag__UJGIqmBPJ78bir6PcY2ERA;
typedef NU32 tySet_tyEnum_TOption__drxda9bT1sSNuJMzO5gmbtA;
typedef NU8 tyEnum_TLocKind__I1OD3pjDLlrlUjTCA9ay87A;
typedef NU8 tyEnum_TStorageLoc__JAK3v9a3NpHrYErEiSn6kUw;
typedef NU16 tySet_tyEnum_TLocFlag__xgqMgSi8aRKtd9aYhl7gDcQ;
struct tyObject_TLoc__xNf0VRJbHsSBgi4wLrInhw {
	tyEnum_TLocKind__I1OD3pjDLlrlUjTCA9ay87A k;
	tyEnum_TStorageLoc__JAK3v9a3NpHrYErEiSn6kUw storage;
	tySet_tyEnum_TLocFlag__xgqMgSi8aRKtd9aYhl7gDcQ flags;
	tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* lode;
	NimStringV2 r;
};
struct tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ {
  tyObject_TIdObj__nIRlm6AIb3flFb9aXcKZjDw Sup;
	tyEnum_TSymKind__N5y2omXzpRTZByIbdpos3g kind;
union{
struct {	tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ* gcUnsafetyReason;
	tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* transformedBody;
} _kind_1;
struct {	tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ* guard;
	NI bitsize;
	NI alignment;
} _kind_2;
};
	tyEnum_TMagic__VHGY7C2GQZGrk80jDSEpVw magic;
	tyObject_TType__hmsWK64eLkzu9axqdsyOa1A* typ;
	tyObject_TIdent__Z9aK6f9cNu2HfiP40JgRCmBw* name;
	tyObject_TLineInfo__7niHWPAAHkAr9aD5TBIThXg info;
	tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ* owner;
	tySet_tyEnum_TSymFlag__UJGIqmBPJ78bir6PcY2ERA flags;
	tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* ast;
	tySet_tyEnum_TOption__drxda9bT1sSNuJMzO5gmbtA options;
	NI position;
	NI32 offset;
	NI32 disamb;
	tyObject_TLoc__xNf0VRJbHsSBgi4wLrInhw loc;
	tyObject_TLib__wawv89bWj8toKwRoAU9c2PWA* annex;
	tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* constraint;
};
struct tySequence__Rb6lnVSG9cMda7VS4gHgKNQ {
  NI len; tySequence__Rb6lnVSG9cMda7VS4gHgKNQ_Content* p;
};
struct tyObject_TNodeTable__w55cv29aPQUsY3ezLC6SPLg {
	NI counter;
	tySequence__Rb6lnVSG9cMda7VS4gHgKNQ data;
};
struct tyObject_TNodePair__0dsX107HfAY9bNJCptoRzaQ {
	NI h;
	tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* key;
	NI val;
};
typedef NU16 tySet_tyEnum_TTypeCmpFlag__EWnbR9bUmV2nwstpPRhrGqg;
struct tySequence__Dna46DaMJFlkp8AvyILKcw_Content { NI cap; tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* data[SEQ_DECL_SIZE]; };
struct tySequence__Rb6lnVSG9cMda7VS4gHgKNQ_Content { NI cap; tyObject_TNodePair__0dsX107HfAY9bNJCptoRzaQ data[SEQ_DECL_SIZE]; };
static N_INLINE(NI, emarkamp___pureZhashes_u4)(NI h_p0, NI val_p1);
N_LIB_PRIVATE N_NIMCALL(NI, toInt__system_u1699)(NF f_p0);
N_LIB_PRIVATE N_NIMCALL(NI, hash__pureZhashes_u300)(NimStringV2 x_p0);
static N_INLINE(NI, len__ast_u3370)(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p0);
N_LIB_PRIVATE N_NIMCALL(NI, hashTree__treetab_u6)(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p0);
static N_INLINE(NI, emarkdollar___pureZhashes_u19)(NI h_p0);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
N_LIB_PRIVATE N_NIMCALL(NI, nodeTableRawGet__treetab_u87)(tyObject_TNodeTable__w55cv29aPQUsY3ezLC6SPLg t_p0, NI k_p1, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* key_p2);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, treesEquivalent__treetab_u39)(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* a_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* b_p1);
static N_INLINE(NIM_BOOL, eqStrings)(NimStringV2 a_p0, NimStringV2 b_p1);
static N_INLINE(NIM_BOOL, equalMem__system_u1748)(void* a_p0, void* b_p1, NI size_p2);
static N_INLINE(int, nimCmpMem)(void* a_p0, void* b_p1, NI size_p2);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, sameTypeOrNil__types_u2216)(tyObject_TType__hmsWK64eLkzu9axqdsyOa1A* a_p0, tyObject_TType__hmsWK64eLkzu9axqdsyOa1A* b_p1, tySet_tyEnum_TTypeCmpFlag__EWnbR9bUmV2nwstpPRhrGqg flags_p2);
static N_INLINE(NI, nextTry__astalgo_u91)(NI h_p0, NI maxHash_p1);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, mustRehash__astalgo_u88)(NI length_p0, NI counter_p1);
N_LIB_PRIVATE N_NIMCALL(tySequence__Rb6lnVSG9cMda7VS4gHgKNQ, newSeq__treetab_u177)(NI len_p0);
N_LIB_PRIVATE N_NIMCALL(void, nodeTableRawInsert__treetab_u134)(tySequence__Rb6lnVSG9cMda7VS4gHgKNQ* data_p0, NI k_p1, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* key_p2, NI val_p3);
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___ast_u3505)(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA** dest_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* src_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqsink___ast_u6382)(tySequence__Rb6lnVSG9cMda7VS4gHgKNQ* dest_p0, tySequence__Rb6lnVSG9cMda7VS4gHgKNQ src_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___ast_u6370)(tySequence__Rb6lnVSG9cMda7VS4gHgKNQ* dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___ast_u6373)(tySequence__Rb6lnVSG9cMda7VS4gHgKNQ dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, nimTestErrorFlag)(void);
extern NIM_BOOL nimInErrorMode__system_u4253;
static N_INLINE(NI, emarkamp___pureZhashes_u4)(NI h_p0, NI val_p1) {
	NI result;
	NU h_2;
	NU val_2;
	NU res;
	h_2 = ((NU) (h_p0));
	val_2 = ((NU) (val_p1));
	res = (NU)((NU64)(h_2) + (NU64)(val_2));
	res = (NU)((NU64)(res) + (NU64)((NU)((NU64)(res) << (NU64)(((NI)10)))));
	res = (NU)(res ^ (NU)((NU64)(res) >> (NU64)(((NI)6))));
	result = ((NI) (res));
	return result;
}
static N_INLINE(NI, len__ast_u3370)(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p0) {
	NI result;
	NI T1_;
	T1_ = (*n_p0)._kind_6.sons.len;
	result = T1_;
	return result;
}
static N_INLINE(NI, emarkdollar___pureZhashes_u19)(NI h_p0) {
	NI result;
	NU h_2;
	NU res;
	h_2 = ((NU) (h_p0));
	res = (NU)((NU64)(h_2) + (NU64)((NU)((NU64)(h_2) << (NU64)(((NI)3)))));
	res = (NU)(res ^ (NU)((NU64)(res) >> (NU64)(((NI)11))));
	res = (NU)((NU64)(res) + (NU64)((NU)((NU64)(res) << (NU64)(((NI)15)))));
	result = ((NI) (res));
	return result;
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4253);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NI, hashTree__treetab_u6)(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p0) {
	NI result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NI)0;
	{
		if (!(n_p0 == 0)) goto LA3_;
		goto BeforeRet_;
	}
LA3_: ;
	result = (*n_p0).kind;
	switch ((*n_p0).kind) {
	case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)1):
	case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)23):
	case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)4):
	{
	}
	break;
	case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)2):
	{
		result = emarkamp___pureZhashes_u4(result, (*(*n_p0)._kind_5.ident).h);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
	break;
	case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)3):
	{
		tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ* xX60gensym0_;
		xX60gensym0_ = NIM_NIL;
		xX60gensym0_ = (*n_p0)._kind_4.sym;
		result = emarkamp___pureZhashes_u4(result, (NI)((NI)((NU64)(((NI) ((*xX60gensym0_).Sup.itemId.module))) << (NU64)(((NI)24))) + ((NI) ((*xX60gensym0_).Sup.itemId.item))));
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
	break;
	case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)5) ... ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)15):
	{
		{
			NIM_BOOL T11_;
			T11_ = (NIM_BOOL)0;
			T11_ = ((IL64(-9223372036854775807) - IL64(1)) <= (*n_p0)._kind_1.intVal);
			if (!(T11_)) goto LA12_;
			T11_ = ((*n_p0)._kind_1.intVal <= IL64(9223372036854775807));
LA12_: ;
			if (!T11_) goto LA13_;
			result = emarkamp___pureZhashes_u4(result, ((NI) ((*n_p0)._kind_1.intVal)));
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		}
LA13_: ;
	}
	break;
	case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)16) ... ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)18):
	{
		{
			NIM_BOOL T18_;
			NI T22_;
			T18_ = (NIM_BOOL)0;
			T18_ = (-1000000.0 <= (*n_p0)._kind_2.floatVal);
			if (!(T18_)) goto LA19_;
			T18_ = ((*n_p0)._kind_2.floatVal <= 1000000.0);
LA19_: ;
			if (!T18_) goto LA20_;
			T22_ = (NI)0;
			T22_ = toInt__system_u1699((*n_p0)._kind_2.floatVal);
			result = emarkamp___pureZhashes_u4(result, T22_);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		}
LA20_: ;
	}
	break;
	case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)20) ... ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)22):
	{
		NI T24_;
		T24_ = (NI)0;
		T24_ = hash__pureZhashes_u300((*n_p0)._kind_3.strVal);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		result = emarkamp___pureZhashes_u4(result, T24_);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
	break;
	default:
	{
		{
			NI i;
			NI colontmp_;
			NI i_2;
			i = (NI)0;
			colontmp_ = (NI)0;
			colontmp_ = len__ast_u3370(n_p0);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			i_2 = ((NI)0);
			{
				while (1) {
					NI T29_;
					if (!(i_2 < colontmp_)) goto LA28;
					i = i_2;
					T29_ = (NI)0;
					T29_ = hashTree__treetab_u6((*n_p0)._kind_6.sons.p->data[i]);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					result = emarkamp___pureZhashes_u4(result, T29_);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					i_2 += ((NI)1);
				} LA28: ;
			}
		}
	}
	break;
	}
	result = emarkdollar___pureZhashes_u19(result);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(int, nimCmpMem)(void* a_p0, void* b_p1, NI size_p2) {
	int result;
	result = memcmp(a_p0, b_p1, ((size_t) (size_p2)));
	return result;
}
static N_INLINE(NIM_BOOL, equalMem__system_u1748)(void* a_p0, void* b_p1, NI size_p2) {
	NIM_BOOL result;
	int T1_;
	T1_ = (int)0;
	T1_ = nimCmpMem(a_p0, b_p1, size_p2);
	result = (T1_ == ((NI32)0));
	return result;
}
static N_INLINE(NIM_BOOL, eqStrings)(NimStringV2 a_p0, NimStringV2 b_p1) {
	NIM_BOOL result;
	NI alen;
	NI blen;
{	result = (NIM_BOOL)0;
	alen = a_p0.len;
	blen = b_p1.len;
	{
		if (!(alen == blen)) goto LA3_;
		{
			if (!(alen == ((NI)0))) goto LA7_;
			result = NIM_TRUE;
			goto BeforeRet_;
		}
LA7_: ;
		result = equalMem__system_u1748(((void*) ((&a_p0.p->data[((NI)0)]))), ((void*) ((&b_p1.p->data[((NI)0)]))), ((NI) (alen)));
		goto BeforeRet_;
	}
LA3_: ;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, treesEquivalent__treetab_u39)(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* a_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* b_p1) {
	NIM_BOOL result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	{
		if (!(a_p0 == b_p1)) goto LA3_;
		result = NIM_TRUE;
	}
	goto LA1_;
LA3_: ;
	{
		NIM_BOOL T6_;
		NIM_BOOL T7_;
		T6_ = (NIM_BOOL)0;
		T7_ = (NIM_BOOL)0;
		T7_ = !((a_p0 == ((tyObject_TNode__dCqWNhWIK3POA9avZBdHukA*) NIM_NIL)));
		if (!(T7_)) goto LA8_;
		T7_ = !((b_p1 == ((tyObject_TNode__dCqWNhWIK3POA9avZBdHukA*) NIM_NIL)));
LA8_: ;
		T6_ = T7_;
		if (!(T6_)) goto LA9_;
		T6_ = ((*a_p0).kind == (*b_p1).kind);
LA9_: ;
		if (!T6_) goto LA10_;
		switch ((*a_p0).kind) {
		case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)1):
		case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)23):
		case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)4):
		{
			result = NIM_TRUE;
		}
		break;
		case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)3):
		{
			tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ* xX60gensym6_;
			tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ* xX60gensym7_;
			xX60gensym6_ = NIM_NIL;
			xX60gensym7_ = NIM_NIL;
			xX60gensym6_ = (*a_p0)._kind_4.sym;
			xX60gensym7_ = (*b_p1)._kind_4.sym;
			result = ((NI)((NI)((NU64)(((NI) ((*xX60gensym6_).Sup.itemId.module))) << (NU64)(((NI)24))) + ((NI) ((*xX60gensym6_).Sup.itemId.item))) == (NI)((NI)((NU64)(((NI) ((*xX60gensym7_).Sup.itemId.module))) << (NU64)(((NI)24))) + ((NI) ((*xX60gensym7_).Sup.itemId.item))));
		}
		break;
		case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)2):
		{
			result = ((*(*a_p0)._kind_5.ident).id == (*(*b_p1)._kind_5.ident).id);
		}
		break;
		case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)5) ... ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)15):
		{
			result = ((*a_p0)._kind_1.intVal == (*b_p1)._kind_1.intVal);
		}
		break;
		case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)16) ... ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)18):
		{
			result = ((*a_p0)._kind_2.floatVal == (*b_p1)._kind_2.floatVal);
		}
		break;
		case ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)20) ... ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)22):
		{
			result = eqStrings((*a_p0)._kind_3.strVal, (*b_p1)._kind_3.strVal);
		}
		break;
		default:
		{
			{
				NI T21_;
				NI T22_;
				T21_ = (NI)0;
				T21_ = len__ast_u3370(a_p0);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				T22_ = (NI)0;
				T22_ = len__ast_u3370(b_p1);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				if (!(T21_ == T22_)) goto LA23_;
				{
					NI i;
					NI colontmp_;
					NI i_2;
					i = (NI)0;
					colontmp_ = (NI)0;
					colontmp_ = len__ast_u3370(a_p0);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					i_2 = ((NI)0);
					{
						while (1) {
							if (!(i_2 < colontmp_)) goto LA27;
							i = i_2;
							{
								NIM_BOOL T30_;
								T30_ = (NIM_BOOL)0;
								T30_ = treesEquivalent__treetab_u39((*a_p0)._kind_6.sons.p->data[i], (*b_p1)._kind_6.sons.p->data[i]);
								if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
								if (!!(T30_)) goto LA31_;
								goto BeforeRet_;
							}
LA31_: ;
							i_2 += ((NI)1);
						} LA27: ;
					}
				}
				result = NIM_TRUE;
			}
LA23_: ;
		}
		break;
		}
		{
			tySet_tyEnum_TTypeCmpFlag__EWnbR9bUmV2nwstpPRhrGqg T37_;
			if (!result) goto LA35_;
			T37_ = 0;
			result = sameTypeOrNil__types_u2216((*a_p0).typ, (*b_p1).typ, T37_);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		}
LA35_: ;
	}
	goto LA1_;
LA10_: ;
LA1_: ;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(NI, nextTry__astalgo_u91)(NI h_p0, NI maxHash_p1) {
	NI result;
	result = (NI)((NI)((NI)(((NI)5) * h_p0) + ((NI)1)) & maxHash_p1);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NI, nodeTableRawGet__treetab_u87)(tyObject_TNodeTable__w55cv29aPQUsY3ezLC6SPLg t_p0, NI k_p1, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* key_p2) {
	NI result;
	NI h;
	NI T1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NI)0;
	T1_ = (t_p0.data.len-1);
	h = (NI)(k_p1 & T1_);
	{
		while (1) {
			NI T10_;
			if (!!((t_p0.data.p->data[h].key == ((tyObject_TNode__dCqWNhWIK3POA9avZBdHukA*) NIM_NIL)))) goto LA3;
			{
				NIM_BOOL T6_;
				T6_ = (NIM_BOOL)0;
				T6_ = (t_p0.data.p->data[h].h == k_p1);
				if (!(T6_)) goto LA7_;
				T6_ = treesEquivalent__treetab_u39(t_p0.data.p->data[h].key, key_p2);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
LA7_: ;
				if (!T6_) goto LA8_;
				result = h;
				goto BeforeRet_;
			}
LA8_: ;
			T10_ = (t_p0.data.len-1);
			h = nextTry__astalgo_u91(h, T10_);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		} LA3: ;
	}
	result = ((NI)-1);
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, nodeTableRawInsert__treetab_u134)(tySequence__Rb6lnVSG9cMda7VS4gHgKNQ* data_p0, NI k_p1, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* key_p2, NI val_p3) {
	NI h;
	NI T1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	T1_ = ((*data_p0).len-1);
	h = (NI)(k_p1 & T1_);
	{
		while (1) {
			NI T4_;
			if (!!(((*data_p0).p->data[h].key == ((tyObject_TNode__dCqWNhWIK3POA9avZBdHukA*) NIM_NIL)))) goto LA3;
			T4_ = ((*data_p0).len-1);
			h = nextTry__astalgo_u91(h, T4_);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		} LA3: ;
	}
	(*data_p0).p->data[h].h = k_p1;
	eqcopy___ast_u3505(&(*data_p0).p->data[h].key, key_p2);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	(*data_p0).p->data[h].val = val_p3;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(NI, nodeTableTestOrSet__treetab_u213)(tyObject_TNodeTable__w55cv29aPQUsY3ezLC6SPLg* t_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* key_p1, NI val_p2) {
	NI result;
	NI k;
	NI index;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NI)0;
	k = hashTree__treetab_u6(key_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	index = nodeTableRawGet__treetab_u87((*t_p0), k, key_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	{
		if (!(((NI)0) <= index)) goto LA3_;
		result = (*t_p0).data.p->data[index].val;
	}
	goto LA1_;
LA3_: ;
	{
		{
			NI T8_;
			NIM_BOOL T9_;
			tySequence__Rb6lnVSG9cMda7VS4gHgKNQ n;
			NI T12_;
			tySequence__Rb6lnVSG9cMda7VS4gHgKNQ T21_;
			T8_ = (*t_p0).data.len;
			T9_ = (NIM_BOOL)0;
			T9_ = mustRehash__astalgo_u88(T8_, (*t_p0).counter);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			if (!T9_) goto LA10_;
			n.len = 0; n.p = NIM_NIL;
			T12_ = (*t_p0).data.len;
			n = newSeq__treetab_u177(((NI) ((NI)(T12_ * ((NI)2)))));
			{
				NI i;
				NI colontmp_;
				NI T14_;
				NI res;
				i = (NI)0;
				colontmp_ = (NI)0;
				T14_ = ((*t_p0).data.len-1);
				colontmp_ = T14_;
				res = ((NI)0);
				{
					while (1) {
						if (!(res <= colontmp_)) goto LA16;
						i = ((NI) (res));
						{
							if (!!(((*t_p0).data.p->data[i].key == ((tyObject_TNode__dCqWNhWIK3POA9avZBdHukA*) NIM_NIL)))) goto LA19_;
							nodeTableRawInsert__treetab_u134((&n), (*t_p0).data.p->data[i].h, (*t_p0).data.p->data[i].key, (*t_p0).data.p->data[i].val);
							if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						}
LA19_: ;
						res += ((NI)1);
					} LA16: ;
				}
			}
			T21_.len = 0; T21_.p = NIM_NIL;
			T21_ = n;
			eqwasMoved___ast_u6370((&n));
			eqsink___ast_u6382((&(*t_p0).data), T21_);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			eqdestroy___ast_u6373(n);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		}
LA10_: ;
		nodeTableRawInsert__treetab_u134((&(*t_p0).data), k, key_p1, val_p2);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		result = val_p2;
		(*t_p0).counter += ((NI)1);
	}
LA1_: ;
	}BeforeRet_: ;
	return result;
}
