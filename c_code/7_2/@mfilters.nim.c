/* Generated by Nim Compiler v2.0.8 */
#define NIM_INTBITS 64

#include "nimbase.h"
#include <stdio.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q;
typedef struct tyObject_TNode__dCqWNhWIK3POA9avZBdHukA tyObject_TNode__dCqWNhWIK3POA9avZBdHukA;
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct tyObject_TType__hmsWK64eLkzu9axqdsyOa1A tyObject_TType__hmsWK64eLkzu9axqdsyOa1A;
typedef struct tyObject_TLineInfo__7niHWPAAHkAr9aD5TBIThXg tyObject_TLineInfo__7niHWPAAHkAr9aD5TBIThXg;
typedef struct tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ;
typedef struct tyObject_TIdent__Z9aK6f9cNu2HfiP40JgRCmBw tyObject_TIdent__Z9aK6f9cNu2HfiP40JgRCmBw;
typedef struct tySequence__Dna46DaMJFlkp8AvyILKcw tySequence__Dna46DaMJFlkp8AvyILKcw;
typedef struct tySequence__Dna46DaMJFlkp8AvyILKcw_Content tySequence__Dna46DaMJFlkp8AvyILKcw_Content;
typedef struct tyTuple__kN8up2W6YKc5YA9avn5mV5w tyTuple__kN8up2W6YKc5YA9avn5mV5w;
typedef struct tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ;
typedef struct RootObj RootObj;
typedef struct TNimTypeV2 TNimTypeV2;
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
struct tyObject_TLineInfo__7niHWPAAHkAr9aD5TBIThXg {
	NU16 line;
	NI16 col;
	NI32 fileIndex;
};
typedef NU32 tySet_tyEnum_TNodeFlag__TQz5bJHIUpAWyJTrllZudg;
typedef NU8 tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw;
struct tySequence__Dna46DaMJFlkp8AvyILKcw {
  NI len; tySequence__Dna46DaMJFlkp8AvyILKcw_Content* p;
};
struct tyObject_TNode__dCqWNhWIK3POA9avZBdHukA {
	tyObject_TType__hmsWK64eLkzu9axqdsyOa1A* typ;
	tyObject_TLineInfo__7niHWPAAHkAr9aD5TBIThXg info;
	tySet_tyEnum_TNodeFlag__TQz5bJHIUpAWyJTrllZudg flags;
	tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw kind;
union{
struct {	NI64 intVal;
} _kind_1;
struct {	NF floatVal;
} _kind_2;
struct {	NimStringV2 strVal;
} _kind_3;
struct {	tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ* sym;
} _kind_4;
struct {	tyObject_TIdent__Z9aK6f9cNu2HfiP40JgRCmBw* ident;
} _kind_5;
struct {	tySequence__Dna46DaMJFlkp8AvyILKcw sons;
} _kind_6;
};
};
typedef NU8 tyEnum_TMsgKind__Lra7DVd4A4ef1Uk8tCF49cA;
typedef NU8 tyEnum_TErrorHandling__seSw1ZrcONMM7pvO9cNwRXg;
typedef NU16 tySet_tyEnum_TRenderFlag__vo80tkPWYbhemWRL89a1vaw;
struct tyTuple__kN8up2W6YKc5YA9avn5mV5w {
NimStringV2 Field0;
NI Field1;
NI Field2;
};
struct tyObject_TIdent__Z9aK6f9cNu2HfiP40JgRCmBw {
	NI id;
	NimStringV2 s;
	tyObject_TIdent__Z9aK6f9cNu2HfiP40JgRCmBw* next;
	NI h;
};
struct TNimTypeV2 {
	void* destructor;
	NI size;
	NI16 align;
	NI16 depth;
	NU32* display;
	void* traceImpl;
	void* typeInfoV1;
	NI flags;
};
struct RootObj {
TNimTypeV2* m_type;
};
typedef NU8 tyEnum_TLLStreamKind__PrSHEZl0oghP1XIed4VaBg;
typedef struct {
N_NIMCALL_PTR(NI, ClP_0) (tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ* s_p0, void* buf_p1, NI bufLen_p2, void* ClE_0);
void* ClE_0;
} tyProc__QWzoYFtkpV8mrj19bay2JGg;
typedef struct {
N_NIMCALL_PTR(void, ClP_0) (void* ClE_0);
void* ClE_0;
} tyProc__HzVCwACFYM9cx9aV62PdjtuA;
struct tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ {
  RootObj Sup;
	tyEnum_TLLStreamKind__PrSHEZl0oghP1XIed4VaBg kind;
	FILE* f;
	NimStringV2 s;
	NI rd;
	NI wr;
	NI lineOffset;
	tyProc__QWzoYFtkpV8mrj19bay2JGg repl;
	tyProc__HzVCwACFYM9cx9aV62PdjtuA onPrompt;
};
typedef NU8 tySet_tyChar__nmiMWKVIe46vacnhAFrQvw[32];
struct tySequence__Dna46DaMJFlkp8AvyILKcw_Content { NI cap; tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* data[SEQ_DECL_SIZE]; };
N_LIB_PRIVATE N_NIMCALL(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA*, getArg__filters_u12)(tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q* conf_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p1, NimStringV2 name_p2, NI pos_p3);
static N_INLINE(NI, len__ast_u3370)(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p0);
N_LIB_PRIVATE N_NIMCALL(void, invalidPragma__filters_u9)(tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q* conf_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p1);
N_LIB_PRIVATE N_NOINLINE(void, liMessage__msgs_u1218)(tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q* conf_p0, tyObject_TLineInfo__7niHWPAAHkAr9aD5TBIThXg info_p1, tyEnum_TMsgKind__Lra7DVd4A4ef1Uk8tCF49cA msg_p2, NimStringV2 arg_p3, tyEnum_TErrorHandling__seSw1ZrcONMM7pvO9cNwRXg eh_p4, tyTuple__kN8up2W6YKc5YA9avn5mV5w* info2_p5, NIM_BOOL isRaw_p6, NIM_BOOL ignoreError_p7);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, nsuFormatSingleElem)(NimStringV2 formatstr_p0, NimStringV2 a_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, renderTree__renderer_u58)(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p0, tySet_tyEnum_TRenderFlag__vo80tkPWYbhemWRL89a1vaw renderFlags_p1);
N_LIB_PRIVATE N_NOCONV(void, dealloc)(void* p_p0);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
N_LIB_PRIVATE N_NIMCALL(NI, nsuCmpIgnoreStyle)(NimStringV2 a_p0, NimStringV2 b_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___ast_u3505)(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA** dest_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* src_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___ast_u3502)(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___stdZassertions_u16)(NimStringV2* dest_p0, NimStringV2 src_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, strArg__filters_u62)(tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q* conf_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p1, NimStringV2 name_p2, NI pos_p3, NimStringV2 default_p4);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, boolArg__filters_u78)(tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q* conf_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p1, NimStringV2 name_p2, NI pos_p3, NIM_BOOL default_p4);
N_LIB_PRIVATE N_NIMCALL(tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ*, llStreamOpen__llstream_u31)(NimStringV2 data_p0);
N_NIMCALL(NimStringV2, rawNewString)(NI cap_p0);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, llStreamReadLine__llstream_u320)(tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ* s_p0, NimStringV2* line_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, nsuStrip)(NimStringV2 s_p0, NIM_BOOL leading_p1, NIM_BOOL trailing_p2, tySet_tyChar__nmiMWKVIe46vacnhAFrQvw chars_p3);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, nsuStartsWith)(NimStringV2 s_p0, NimStringV2 prefix_p1);
N_LIB_PRIVATE N_NIMCALL(void, llStreamWriteln__llstream_u352)(tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ* s_p0, NimStringV2 data_p1);
N_LIB_PRIVATE N_NIMCALL(void, llStreamClose__llstream_u228)(tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ* s_p0);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, nsuReplaceStr)(NimStringV2 s_p0, NimStringV2 sub_p1, NimStringV2 by_p2);
N_LIB_PRIVATE N_NIMCALL(void, nimTestErrorFlag)(void);
static const struct {
  NI cap; NIM_CHAR data[21+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_2 = { 21 | NIM_STRLIT_FLAG, "\'$1\' not allowed here" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_3 = {21, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_2};
static const struct {
  NI cap; NIM_CHAR data[62+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_5 = { 62 | NIM_STRLIT_FLAG, "/home/runner/work/nightlies/nightlies/nim/compiler/filters.nim" };
static const struct {
  NI cap; NIM_CHAR data[10+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_6 = { 10 | NIM_STRLIT_FLAG, "startswith" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_7 = {10, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_6};
static const struct {
  NI cap; NIM_CHAR data[0+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_8 = { 0 | NIM_STRLIT_FLAG, "" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_9 = {0, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_8};
static const struct {
  NI cap; NIM_CHAR data[4+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_10 = { 4 | NIM_STRLIT_FLAG, "true" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_11 = {4, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_10};
static const struct {
  NI cap; NIM_CHAR data[5+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_12 = { 5 | NIM_STRLIT_FLAG, "false" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_13 = {5, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_12};
static const struct {
  NI cap; NIM_CHAR data[7+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_14 = { 7 | NIM_STRLIT_FLAG, "leading" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_15 = {7, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_14};
static const struct {
  NI cap; NIM_CHAR data[8+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_16 = { 8 | NIM_STRLIT_FLAG, "trailing" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_17 = {8, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_16};
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_18 = {0, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_8};
static NIM_CONST tySet_tyChar__nmiMWKVIe46vacnhAFrQvw TM__TpC7tIfhGAaosc7HIsUuXA_19 = {
0x00, 0x3e, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
;
static const struct {
  NI cap; NIM_CHAR data[3+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_20 = { 3 | NIM_STRLIT_FLAG, "sub" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_21 = {3, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_20};
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_22 = {0, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_8};
static const struct {
  NI cap; NIM_CHAR data[2+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_23 = { 2 | NIM_STRLIT_FLAG, "by" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_24 = {2, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_23};
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_25 = {0, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_8};
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_26 = {0, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_8};
static NIM_CONST tyTuple__kN8up2W6YKc5YA9avn5mV5w TM__TpC7tIfhGAaosc7HIsUuXA_4 = {{62, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_5},
((NI)17),
((NI)12)}
;
extern NIM_BOOL nimInErrorMode__system_u4215;
static N_INLINE(NI, len__ast_u3370)(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p0) {
	NI result;
	NI T1_;
	T1_ = (*n_p0)._kind_6.sons.len;
	result = T1_;
	return result;
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4215);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, invalidPragma__filters_u9)(tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q* conf_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p1) {
	NimStringV2 colontmpD_;
	NimStringV2 T2_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
	T2_.len = 0; T2_.p = NIM_NIL;
	T2_ = renderTree__renderer_u58(n_p1, 4);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	colontmpD_ = nsuFormatSingleElem(TM__TpC7tIfhGAaosc7HIsUuXA_3, T2_);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	liMessage__msgs_u1218(conf_p0, (*n_p1).info, ((tyEnum_TMsgKind__Lra7DVd4A4ef1Uk8tCF49cA)17), colontmpD_, ((tyEnum_TErrorHandling__seSw1ZrcONMM7pvO9cNwRXg)0), (&TM__TpC7tIfhGAaosc7HIsUuXA_4), NIM_FALSE, NIM_FALSE);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		LA1_:;
	}
	{
		if (colontmpD_.p && !(colontmpD_.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(colontmpD_.p);
}
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TNode__dCqWNhWIK3POA9avZBdHukA*, getArg__filters_u12)(tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q* conf_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p1, NimStringV2 name_p2, NI pos_p3) {
	tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = ((tyObject_TNode__dCqWNhWIK3POA9avZBdHukA*) NIM_NIL);
	{
		if (!((*n_p1).kind >= ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)1) && (*n_p1).kind <= ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)23))) goto LA3_;
		goto BeforeRet_;
	}
LA3_: ;
	{
		NI i;
		NI colontmp_;
		NI i_2;
		i = (NI)0;
		colontmp_ = (NI)0;
		colontmp_ = len__ast_u3370(n_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		i_2 = ((NI)1);
		{
			while (1) {
				if (!(i_2 < colontmp_)) goto LA7;
				i = i_2;
				{
					if (!((*(*n_p1)._kind_6.sons.p->data[i]).kind == ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)33))) goto LA10_;
					{
						if (!!(((*(*(*n_p1)._kind_6.sons.p->data[i])._kind_6.sons.p->data[((NI)0)]).kind == ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)2)))) goto LA14_;
						invalidPragma__filters_u9(conf_p0, n_p1);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					}
LA14_: ;
					{
						NI T18_;
						T18_ = (NI)0;
						T18_ = nsuCmpIgnoreStyle((*(*(*(*n_p1)._kind_6.sons.p->data[i])._kind_6.sons.p->data[((NI)0)])._kind_5.ident).s, name_p2);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						if (!(T18_ == ((NI)0))) goto LA19_;
						eqcopy___ast_u3505(&result, (*(*n_p1)._kind_6.sons.p->data[i])._kind_6.sons.p->data[((NI)1)]);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						goto BeforeRet_;
					}
LA19_: ;
				}
				goto LA8_;
LA10_: ;
				{
					if (!(i == pos_p3)) goto LA22_;
					eqcopy___ast_u3505(&result, (*n_p1)._kind_6.sons.p->data[i]);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					goto BeforeRet_;
				}
				goto LA8_;
LA22_: ;
LA8_: ;
				i_2 += ((NI)1);
			} LA7: ;
		}
	}
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NIM_CHAR, charArg__filters_u46)(tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q* conf_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p1, NimStringV2 name_p2, NI pos_p3, NIM_CHAR default_p4) {
	NIM_CHAR result;
	tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* x;
NIM_BOOL oldNimErrFin1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_CHAR)0;
	x = NIM_NIL;
	x = getArg__filters_u12(conf_p0, n_p1, name_p2, pos_p3);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		if (!(x == ((tyObject_TNode__dCqWNhWIK3POA9avZBdHukA*) NIM_NIL))) goto LA4_;
		result = default_p4;
	}
	goto LA2_;
LA4_: ;
	{
		if (!((*x).kind == ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)5))) goto LA7_;
		result = ((NIM_CHAR) (((NI) (((NI) ((*x)._kind_1.intVal))))));
	}
	goto LA2_;
LA7_: ;
	{
		invalidPragma__filters_u9(conf_p0, n_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	}
LA2_: ;
	{
		LA1_:;
	}
	{
		oldNimErrFin1_ = *nimErr_; *nimErr_ = NIM_FALSE;
		eqdestroy___ast_u3502(x);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		*nimErr_ = oldNimErrFin1_;
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, strArg__filters_u62)(tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q* conf_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p1, NimStringV2 name_p2, NI pos_p3, NimStringV2 default_p4) {
	NimStringV2 result;
	tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* x;
NIM_BOOL oldNimErrFin1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result.len = 0; result.p = NIM_NIL;
	x = NIM_NIL;
	x = getArg__filters_u12(conf_p0, n_p1, name_p2, pos_p3);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		if (!(x == ((tyObject_TNode__dCqWNhWIK3POA9avZBdHukA*) NIM_NIL))) goto LA4_;
		eqcopy___stdZassertions_u16((&result), default_p4);
	}
	goto LA2_;
LA4_: ;
	{
		if (!((*x).kind >= ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)20) && (*x).kind <= ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)22))) goto LA7_;
		eqcopy___stdZassertions_u16((&result), (*x)._kind_3.strVal);
	}
	goto LA2_;
LA7_: ;
	{
		invalidPragma__filters_u9(conf_p0, n_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	}
LA2_: ;
	{
		LA1_:;
	}
	{
		oldNimErrFin1_ = *nimErr_; *nimErr_ = NIM_FALSE;
		eqdestroy___ast_u3502(x);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		*nimErr_ = oldNimErrFin1_;
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, boolArg__filters_u78)(tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q* conf_p0, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* n_p1, NimStringV2 name_p2, NI pos_p3, NIM_BOOL default_p4) {
	NIM_BOOL result;
	tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* x;
NIM_BOOL oldNimErrFin1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	x = NIM_NIL;
	x = getArg__filters_u12(conf_p0, n_p1, name_p2, pos_p3);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		if (!(x == ((tyObject_TNode__dCqWNhWIK3POA9avZBdHukA*) NIM_NIL))) goto LA4_;
		result = default_p4;
	}
	goto LA2_;
LA4_: ;
	{
		NIM_BOOL T7_;
		NI T9_;
		T7_ = (NIM_BOOL)0;
		T7_ = ((*x).kind == ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)2));
		if (!(T7_)) goto LA8_;
		T9_ = (NI)0;
		T9_ = nsuCmpIgnoreStyle((*(*x)._kind_5.ident).s, TM__TpC7tIfhGAaosc7HIsUuXA_11);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
		T7_ = (T9_ == ((NI)0));
LA8_: ;
		if (!T7_) goto LA10_;
		result = NIM_TRUE;
	}
	goto LA2_;
LA10_: ;
	{
		NIM_BOOL T13_;
		NI T15_;
		T13_ = (NIM_BOOL)0;
		T13_ = ((*x).kind == ((tyEnum_TNodeKind__bcbcDqLTSU62g7oGi9c1Kdw)2));
		if (!(T13_)) goto LA14_;
		T15_ = (NI)0;
		T15_ = nsuCmpIgnoreStyle((*(*x)._kind_5.ident).s, TM__TpC7tIfhGAaosc7HIsUuXA_13);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
		T13_ = (T15_ == ((NI)0));
LA14_: ;
		if (!T13_) goto LA16_;
		result = NIM_FALSE;
	}
	goto LA2_;
LA16_: ;
	{
		invalidPragma__filters_u9(conf_p0, n_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	}
LA2_: ;
	{
		LA1_:;
	}
	{
		oldNimErrFin1_ = *nimErr_; *nimErr_ = NIM_FALSE;
		eqdestroy___ast_u3502(x);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		*nimErr_ = oldNimErrFin1_;
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ*, filterStrip__filters_u98)(tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q* conf_p0, tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ* stdin_p1, NimStringV2 filename_p2, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* call_p3) {
	tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ* result;
	NimStringV2 pattern;
	NimStringV2 line;
	NIM_BOOL leading;
	NIM_BOOL trailing;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = NIM_NIL;
	pattern.len = 0; pattern.p = NIM_NIL;
	line.len = 0; line.p = NIM_NIL;
	pattern = strArg__filters_u62(conf_p0, call_p3, TM__TpC7tIfhGAaosc7HIsUuXA_7, ((NI)1), TM__TpC7tIfhGAaosc7HIsUuXA_9);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	leading = boolArg__filters_u78(conf_p0, call_p3, TM__TpC7tIfhGAaosc7HIsUuXA_15, ((NI)2), NIM_TRUE);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	trailing = boolArg__filters_u78(conf_p0, call_p3, TM__TpC7tIfhGAaosc7HIsUuXA_17, ((NI)3), NIM_TRUE);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	result = llStreamOpen__llstream_u31(TM__TpC7tIfhGAaosc7HIsUuXA_18);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	line = rawNewString(((NI)80));
	{
		while (1) {
			NIM_BOOL T4_;
			NimStringV2 stripped;
			T4_ = (NIM_BOOL)0;
			T4_ = llStreamReadLine__llstream_u320(stdin_p1, (&line));
			if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			if (!T4_) goto LA3;
			stripped.len = 0; stripped.p = NIM_NIL;
			stripped = nsuStrip(line, leading, trailing, TM__TpC7tIfhGAaosc7HIsUuXA_19);
			if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
			{
				NIM_BOOL T8_;
				T8_ = (NIM_BOOL)0;
				T8_ = (pattern.len == ((NI)0));
				if (T8_) goto LA9_;
				T8_ = nsuStartsWith(stripped, pattern);
				if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
LA9_: ;
				if (!T8_) goto LA10_;
				llStreamWriteln__llstream_u352(result, stripped);
				if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
			}
			goto LA6_;
LA10_: ;
			{
				llStreamWriteln__llstream_u352(result, line);
				if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
			}
LA6_: ;
			{
				LA5_:;
			}
			{
				if (stripped.p && !(stripped.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(stripped.p);
}
			}
			if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
		} LA3: ;
	}
	llStreamClose__llstream_u228(stdin_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		LA1_:;
	}
	{
		if (line.p && !(line.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(line.p);
}
		if (pattern.p && !(pattern.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(pattern.p);
}
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ*, filterReplace__filters_u109)(tyObject_ConfigRefcolonObjectType___8ircDcMFPbrQpqVoquee0Q* conf_p0, tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ* stdin_p1, NimStringV2 filename_p2, tyObject_TNode__dCqWNhWIK3POA9avZBdHukA* call_p3) {
	tyObject_TLLStream__3vcaT9b2SnJwuWUcu3NvYHQ* result;
	NimStringV2 sub;
	NimStringV2 by;
	NimStringV2 line;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = NIM_NIL;
	sub.len = 0; sub.p = NIM_NIL;
	by.len = 0; by.p = NIM_NIL;
	line.len = 0; line.p = NIM_NIL;
	sub = strArg__filters_u62(conf_p0, call_p3, TM__TpC7tIfhGAaosc7HIsUuXA_21, ((NI)1), TM__TpC7tIfhGAaosc7HIsUuXA_22);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		if (!(sub.len == ((NI)0))) goto LA4_;
		invalidPragma__filters_u9(conf_p0, call_p3);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	}
LA4_: ;
	by = strArg__filters_u62(conf_p0, call_p3, TM__TpC7tIfhGAaosc7HIsUuXA_24, ((NI)2), TM__TpC7tIfhGAaosc7HIsUuXA_25);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	result = llStreamOpen__llstream_u31(TM__TpC7tIfhGAaosc7HIsUuXA_26);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	line = rawNewString(((NI)80));
	{
		while (1) {
			NIM_BOOL T8_;
			NimStringV2 colontmpD_;
			T8_ = (NIM_BOOL)0;
			T8_ = llStreamReadLine__llstream_u320(stdin_p1, (&line));
			if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			if (!T8_) goto LA7;
			colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
			colontmpD_ = nsuReplaceStr(line, sub, by);
			if (NIM_UNLIKELY(*nimErr_)) goto LA9_;
			llStreamWriteln__llstream_u352(result, colontmpD_);
			if (NIM_UNLIKELY(*nimErr_)) goto LA9_;
			{
				LA9_:;
			}
			{
				if (colontmpD_.p && !(colontmpD_.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(colontmpD_.p);
}
			}
			if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
		} LA7: ;
	}
	llStreamClose__llstream_u228(stdin_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		LA1_:;
	}
	{
		if (line.p && !(line.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(line.p);
}
		if (by.p && !(by.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(by.p);
}
		if (sub.p && !(sub.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(sub.p);
}
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
