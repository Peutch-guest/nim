/* Generated by Nim Compiler v2.0.8 */
#define NIM_INTBITS 64
#define NIM_EmulateOverflowChecks

#include "nimbase.h"
#include <string.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct tyObject_SinglyLinkedList__g3P77tPvg1FlUlZH8ySw8Q tyObject_SinglyLinkedList__g3P77tPvg1FlUlZH8ySw8Q;
typedef struct tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA;
typedef struct tyTuple__KPVKrfx82Va9azSnb0dl1RQ tyTuple__KPVKrfx82Va9azSnb0dl1RQ;
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct TNimTypeV2 TNimTypeV2;
typedef struct tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA;
typedef struct tyObject_GcEnv__Is9c2EwqdTVYS5IixBEGPJA tyObject_GcEnv__Is9c2EwqdTVYS5IixBEGPJA;
typedef struct tyObject_CellSeq__Hfew3G9anq5rd8HeaoZ5D8A tyObject_CellSeq__Hfew3G9anq5rd8HeaoZ5D8A;
typedef struct tyTuple__N4J9cV4JZGem3ljqqj5rT0Q tyTuple__N4J9cV4JZGem3ljqqj5rT0Q;
typedef struct tyObject_CellSeq__YGBDRaqXdn9cakJuKqiHgcQ tyObject_CellSeq__YGBDRaqXdn9cakJuKqiHgcQ;
typedef struct tyTuple__9aMkUW3U5CHmbLkYPT2CpMw tyTuple__9aMkUW3U5CHmbLkYPT2CpMw;
struct tyObject_SinglyLinkedList__g3P77tPvg1FlUlZH8ySw8Q {
	tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* head;
	tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* tail;
};
typedef NU8 tyEnum_FootnoteType__jXnI8xEO3SXxVR9afYCJnyQ;
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
struct tyTuple__KPVKrfx82Va9azSnb0dl1RQ {
tyEnum_FootnoteType__jXnI8xEO3SXxVR9afYCJnyQ Field0;
NI Field1;
NI Field2;
NI Field3;
NimStringV2 Field4;
};
struct tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA {
	tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* next;
	tyTuple__KPVKrfx82Va9azSnb0dl1RQ value;
};
struct tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA {
	NI rc;
	NI rootIdx;
};
struct tyObject_CellSeq__Hfew3G9anq5rd8HeaoZ5D8A {
	NI len;
	NI cap;
	tyTuple__N4J9cV4JZGem3ljqqj5rT0Q* d;
};
struct tyObject_CellSeq__YGBDRaqXdn9cakJuKqiHgcQ {
	NI len;
	NI cap;
	tyTuple__9aMkUW3U5CHmbLkYPT2CpMw* d;
};
struct tyObject_GcEnv__Is9c2EwqdTVYS5IixBEGPJA {
	tyObject_CellSeq__Hfew3G9anq5rd8HeaoZ5D8A traceStack;
	tyObject_CellSeq__YGBDRaqXdn9cakJuKqiHgcQ toFree;
	NI freed;
	NI touched;
	NI edges;
	NI rcSum;
	NIM_BOOL keepThreshold;
};
struct tyTuple__N4J9cV4JZGem3ljqqj5rT0Q {
void** Field0;
TNimTypeV2* Field1;
};
struct TNimTypeV2 {
	void* destructor;
	NI size;
	NI16 align;
	NI16 depth;
	NU32* display;
	void* traceImpl;
	void* typeInfoV1;
	NI flags;
};
static N_INLINE(void, nimZeroMem)(void* p_p0, NI size_p1);
static N_INLINE(void, nimSetMem__systemZmemory_u7)(void* a_p0, int v_p1, NI size_p2);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
static N_INLINE(void, add__packagesZdocutilsZrst_u8664)(tyObject_SinglyLinkedList__g3P77tPvg1FlUlZH8ySw8Q* L_p0, tyTuple__KPVKrfx82Va9azSnb0dl1RQ* value_p1);
static N_INLINE(void, add__packagesZdocutilsZrst_u8822)(tyObject_SinglyLinkedList__g3P77tPvg1FlUlZH8ySw8Q* L_p0, tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* n_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqsink___packagesZdocutilsZrst_u8693)(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA** dest_p0, tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* src_p1);
static N_INLINE(NIM_BOOL, nimDecRefIsLastCyclicStatic)(void* p_p0, TNimTypeV2* desc_p1);
static N_INLINE(NI, minuspercent___system_u810)(NI x_p0, NI y_p1);
N_LIB_PRIVATE N_NOINLINE(void, rememberCycle__system_u3452)(NIM_BOOL isDestroyAction_p0, tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA* s_p1, TNimTypeV2* desc_p2);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___packagesZdocutilsZrst_u8702)(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, nimRawDispose)(void* p_p0, NI alignment_p1);
N_LIB_PRIVATE N_NOCONV(void, dealloc)(void* p_p0);
static N_INLINE(void, nimTraceRef)(void* q_p0, TNimTypeV2* desc_p1, void* env_p2);
static N_INLINE(void, add__system_u2916)(tyObject_CellSeq__Hfew3G9anq5rd8HeaoZ5D8A* s_p0, void** c_p1, TNimTypeV2* t_p2);
N_LIB_PRIVATE N_NIMCALL(void, resize__system_u2924)(tyObject_CellSeq__Hfew3G9anq5rd8HeaoZ5D8A* s_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___packagesZdocutilsZrst_u8685)(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA** dest_p0, tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* src_p1, NIM_BOOL cyclic_p2);
static N_INLINE(void, nimIncRefCyclic)(void* p_p0, NIM_BOOL cyclic_p1);
N_LIB_PRIVATE N_NIMCALL(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA*, newSinglyLinkedNode__packagesZdocutilsZrst_u8672)(tyTuple__KPVKrfx82Va9azSnb0dl1RQ* value_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___packagesZdocutilsZrst_u8682)(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* dest_p0);
N_LIB_PRIVATE N_NIMCALL(void*, nimNewObj)(NI size_p0, NI alignment_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___packagesZdocutilsZrst_u3568)(tyTuple__KPVKrfx82Va9azSnb0dl1RQ* dest_p0, tyTuple__KPVKrfx82Va9azSnb0dl1RQ* src_p1);
N_LIB_PRIVATE N_NIMCALL(void, nimTestErrorFlag)(void);
extern TNimTypeV2 NTIv2__dRlnkNHLLKxta2PiHXbXzA_;
extern NIM_BOOL nimInErrorMode__system_u4238;
static N_INLINE(void, nimSetMem__systemZmemory_u7)(void* a_p0, int v_p1, NI size_p2) {
	void* T1_;
	T1_ = (void*)0;
	T1_ = memset(a_p0, v_p1, ((size_t) (size_p2)));
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4238);
	return result;
}
static N_INLINE(void, nimZeroMem)(void* p_p0, NI size_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	nimSetMem__systemZmemory_u7(p_p0, ((int)0), size_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, initSinglyLinkedList__packagesZdocutilsZrst_u8562)(tyObject_SinglyLinkedList__g3P77tPvg1FlUlZH8ySw8Q* Result) {
	nimZeroMem((void*)Result, sizeof(tyObject_SinglyLinkedList__g3P77tPvg1FlUlZH8ySw8Q));
}
static N_INLINE(NI, minuspercent___system_u810)(NI x_p0, NI y_p1) {
	NI result;
	result = ((NI) ((NU)((NU64)(((NU) (x_p0))) - (NU64)(((NU) (y_p1))))));
	return result;
}
static N_INLINE(NIM_BOOL, nimDecRefIsLastCyclicStatic)(void* p_p0, TNimTypeV2* desc_p1) {
	NIM_BOOL result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	{
		tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA* cell;
		NI T5_;
		if (!!((p_p0 == NIM_NIL))) goto LA3_;
		T5_ = (NI)0;
		T5_ = minuspercent___system_u810(((NI) (ptrdiff_t) (p_p0)), ((NI)16));
		cell = ((tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA*) (T5_));
		{
			if (!((NI)((*cell).rc & ((NI)-16)) == ((NI)0))) goto LA8_;
			result = NIM_TRUE;
		}
		goto LA6_;
LA8_: ;
		{
			(*cell).rc -= ((NI)16);
		}
LA6_: ;
		rememberCycle__system_u3452(result, cell, desc_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
LA3_: ;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___packagesZdocutilsZrst_u8702)(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* dest_p0) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = nimDecRefIsLastCyclicStatic((*dest_p0).next, (&NTIv2__dRlnkNHLLKxta2PiHXbXzA_));
		if (!T3_) goto LA4_;
		eqdestroy___packagesZdocutilsZrst_u8702((*dest_p0).next);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		nimRawDispose((*dest_p0).next, ((NI)8));
	}
LA4_: ;
	if ((*dest_p0).value.Field4.p && !((*dest_p0).value.Field4.p->cap & NIM_STRLIT_FLAG)) {
 dealloc((*dest_p0).value.Field4.p);
}
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, rttiDestroy__packagesZdocutilsZrst_u18520)(void* dest_p0) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	eqdestroy___packagesZdocutilsZrst_u8702(((tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA*) (dest_p0)));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
static N_INLINE(void, add__system_u2916)(tyObject_CellSeq__Hfew3G9anq5rd8HeaoZ5D8A* s_p0, void** c_p1, TNimTypeV2* t_p2) {
	void** colontmp_;
	TNimTypeV2* colontmp__2;
	{
		if (!((*s_p0).cap <= (*s_p0).len)) goto LA3_;
		resize__system_u2924(s_p0);
	}
LA3_: ;
	colontmp_ = c_p1;
	colontmp__2 = t_p2;
	(*s_p0).d[(*s_p0).len].Field0 = colontmp_;
	(*s_p0).d[(*s_p0).len].Field1 = colontmp__2;
	(*s_p0).len += ((NI)1);
}
static N_INLINE(void, nimTraceRef)(void* q_p0, TNimTypeV2* desc_p1, void* env_p2) {
	void** p;
	p = ((void**) (q_p0));
	{
		tyObject_GcEnv__Is9c2EwqdTVYS5IixBEGPJA* j;
		if (!!(((*p) == NIM_NIL))) goto LA3_;
		j = ((tyObject_GcEnv__Is9c2EwqdTVYS5IixBEGPJA*) (env_p2));
		add__system_u2916((&(*j).traceStack), p, desc_p1);
	}
LA3_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, eqtrace___packagesZdocutilsZrst_u8714)(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* dest_p0, void* env_p1) {
	nimTraceRef(&(*dest_p0).next, (&NTIv2__dRlnkNHLLKxta2PiHXbXzA_), env_p1);
}
N_LIB_PRIVATE N_NIMCALL(void, eqsink___packagesZdocutilsZrst_u8693)(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA** dest_p0, tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* src_p1) {
	tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* colontmp_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	colontmp_ = (*dest_p0);
	(*dest_p0) = src_p1;
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = nimDecRefIsLastCyclicStatic(colontmp_, (&NTIv2__dRlnkNHLLKxta2PiHXbXzA_));
		if (!T3_) goto LA4_;
		eqdestroy___packagesZdocutilsZrst_u8702(colontmp_);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		nimRawDispose(colontmp_, ((NI)8));
	}
LA4_: ;
	}BeforeRet_: ;
}
static N_INLINE(void, nimIncRefCyclic)(void* p_p0, NIM_BOOL cyclic_p1) {
	tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA* h;
	NI T1_;
	T1_ = (NI)0;
	T1_ = minuspercent___system_u810(((NI) (ptrdiff_t) (p_p0)), ((NI)16));
	h = ((tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA*) (T1_));
	(*h).rc += ((NI)16);
}
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___packagesZdocutilsZrst_u8685)(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA** dest_p0, tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* src_p1, NIM_BOOL cyclic_p2) {
	tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* colontmp_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	colontmp_ = (*dest_p0);
	{
		if (!src_p1) goto LA3_;
		nimIncRefCyclic(src_p1, cyclic_p2);
	}
LA3_: ;
	(*dest_p0) = src_p1;
	{
		NIM_BOOL T7_;
		T7_ = (NIM_BOOL)0;
		T7_ = nimDecRefIsLastCyclicStatic(colontmp_, (&NTIv2__dRlnkNHLLKxta2PiHXbXzA_));
		if (!T7_) goto LA8_;
		eqdestroy___packagesZdocutilsZrst_u8702(colontmp_);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		nimRawDispose(colontmp_, ((NI)8));
	}
LA8_: ;
	}BeforeRet_: ;
}
static N_INLINE(void, add__packagesZdocutilsZrst_u8822)(tyObject_SinglyLinkedList__g3P77tPvg1FlUlZH8ySw8Q* L_p0, tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* n_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	eqsink___packagesZdocutilsZrst_u8693(&(*n_p1).next, ((tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA*) NIM_NIL));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	{
		if (!!(((*L_p0).tail == ((tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA*) NIM_NIL)))) goto LA3_;
		eqcopy___packagesZdocutilsZrst_u8685(&(*(*L_p0).tail).next, n_p1, NIM_TRUE);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
LA3_: ;
	(*L_p0).tail = n_p1;
	{
		if (!((*L_p0).head == ((tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA*) NIM_NIL))) goto LA7_;
		eqcopy___packagesZdocutilsZrst_u8685(&(*L_p0).head, n_p1, NIM_TRUE);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
LA7_: ;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___packagesZdocutilsZrst_u8682)(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* dest_p0) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = nimDecRefIsLastCyclicStatic(dest_p0, (&NTIv2__dRlnkNHLLKxta2PiHXbXzA_));
		if (!T3_) goto LA4_;
		eqdestroy___packagesZdocutilsZrst_u8702(dest_p0);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		nimRawDispose(dest_p0, ((NI)8));
	}
LA4_: ;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA*, newSinglyLinkedNode__packagesZdocutilsZrst_u8672)(tyTuple__KPVKrfx82Va9azSnb0dl1RQ* value_p0) {
	tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = NIM_NIL;
	eqdestroy___packagesZdocutilsZrst_u8682(result);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	result = (tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA*) nimNewObj(sizeof(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA), NIM_ALIGNOF(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA));
	eqcopy___packagesZdocutilsZrst_u3568((&(*result).value), value_p0);
	}BeforeRet_: ;
	return result;
}
static N_INLINE(void, add__packagesZdocutilsZrst_u8664)(tyObject_SinglyLinkedList__g3P77tPvg1FlUlZH8ySw8Q* L_p0, tyTuple__KPVKrfx82Va9azSnb0dl1RQ* value_p1) {
	tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* colontmpD_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	colontmpD_ = NIM_NIL;
	colontmpD_ = newSinglyLinkedNode__packagesZdocutilsZrst_u8672(value_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	add__packagesZdocutilsZrst_u8822(L_p0, colontmpD_);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	eqdestroy___packagesZdocutilsZrst_u8682(colontmpD_);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, append__packagesZdocutilsZrst_u8589)(tyObject_SinglyLinkedList__g3P77tPvg1FlUlZH8ySw8Q* a_p0, tyTuple__KPVKrfx82Va9azSnb0dl1RQ* b_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	add__packagesZdocutilsZrst_u8664(a_p0, b_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA*, newSinglyLinkedNode__packagesZdocutilsZrst_u9210)(tyTuple__KPVKrfx82Va9azSnb0dl1RQ* value_p0) {
	tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA* result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = NIM_NIL;
	eqdestroy___packagesZdocutilsZrst_u8682(result);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	result = (tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA*) nimNewObj(sizeof(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA), NIM_ALIGNOF(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA));
	eqcopy___packagesZdocutilsZrst_u3568((&(*result).value), value_p0);
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___packagesZdocutilsZrst_u8679)(tyObject_SinglyLinkedNodeObj__dRlnkNHLLKxta2PiHXbXzA** dest_p0) {
	(*dest_p0) = 0;
}
N_LIB_PRIVATE N_NIMCALL(void, append__packagesZdocutilsZrst_u9275)(tyObject_SinglyLinkedList__g3P77tPvg1FlUlZH8ySw8Q* a_p0, tyTuple__KPVKrfx82Va9azSnb0dl1RQ* b_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	add__packagesZdocutilsZrst_u8664(a_p0, b_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___packagesZdocutilsZrst_u9503)(tyObject_SinglyLinkedList__g3P77tPvg1FlUlZH8ySw8Q* dest_p0) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = nimDecRefIsLastCyclicStatic((*dest_p0).head, (&NTIv2__dRlnkNHLLKxta2PiHXbXzA_));
		if (!T3_) goto LA4_;
		eqdestroy___packagesZdocutilsZrst_u8702((*dest_p0).head);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		nimRawDispose((*dest_p0).head, ((NI)8));
	}
LA4_: ;
	}BeforeRet_: ;
}
