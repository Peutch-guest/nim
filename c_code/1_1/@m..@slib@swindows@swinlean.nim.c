/* Generated by Nim Compiler v2.0.8 */
#define NIM_INTBITS 32

#include "nimbase.h"
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct tyObject_GUID__KhSF19anzUEjuSNqxodiVvQ tyObject_GUID__KhSF19anzUEjuSNqxodiVvQ;
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct tyObject_WIN32_FIND_DATA__U9aHFJYS6OptpDrgTrpKXbQ tyObject_WIN32_FIND_DATA__U9aHFJYS6OptpDrgTrpKXbQ;
typedef struct tyObject_SECURITY_ATTRIBUTES__uDMqUzOPuH1lv6sAMGbeUQ tyObject_SECURITY_ATTRIBUTES__uDMqUzOPuH1lv6sAMGbeUQ;
typedef struct tyObject_STARTUPINFO__WgdDLv9bNl1jYz9blfSJnoyw tyObject_STARTUPINFO__WgdDLv9bNl1jYz9blfSJnoyw;
typedef struct tyObject_PROCESS_INFORMATION__rtX62MtURAW2J5XCXdN9c3g tyObject_PROCESS_INFORMATION__rtX62MtURAW2J5XCXdN9c3g;
typedef struct tyObject_FILETIME__tx5ctSQ9aLxkzr39cl7M9cXrQ tyObject_FILETIME__tx5ctSQ9aLxkzr39cl7M9cXrQ;
typedef NI8 tyArray__9cWG514ToTTjfTPLhXXV0IQ[8];
struct tyObject_GUID__KhSF19anzUEjuSNqxodiVvQ {
	NI32 D1;
	NI16 D2;
	NI16 D3;
	tyArray__9cWG514ToTTjfTPLhXXV0IQ D4;
};
typedef N_STDCALL_PTR(NCSTRING, tyProc__7LMcD5Rujqk7H2JqdJtoyA) (int family_p0, void* paddr_p1, NCSTRING pStringBuffer_p2, NI32 stringBufSize_p3);
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
typedef N_STDCALL_PTR(NI16*, tyProc__9bXHKfH08cKXphxNi5GYejg) (void);
typedef N_STDCALL_PTR(NI32, tyProc__aMOow6Lp4fo8dW2ko9aU3pg) (NI32 nBufferLength_p0, NI16* lpBuffer_p1);
typedef N_STDCALL_PTR(NI32, tyProc__aO673xGTLLxou7P7GxoCXA) (NI32 dwFlags_p0, void* lpSource_p1, NI32 dwMessageId_p2, NI32 dwLanguageId_p3, void* lpBuffer_p4, NI32 nSize_p5, void* arguments_p6);
typedef N_STDCALL_PTR(void, tyProc__im9buRnIvptJfzdASYMEbBA) (void* p_p0);
typedef N_STDCALL_PTR(NI32, tyProc__9bXer9a4ps9aSGctILcxWReVw) (void);
typedef N_STDCALL_PTR(NI, tyProc__mLqKqb2iQ7G5ICPX33ZfZg) (NI16* lpFileName_p0, tyObject_WIN32_FIND_DATA__U9aHFJYS6OptpDrgTrpKXbQ* lpFindFileData_p1);
typedef N_STDCALL_PTR(void, tyProc__ofoySXaAAlxxs9bQS9a1etlg) (NI hFindFile_p0);
typedef N_STDCALL_PTR(NI32, tyProc__h9aq7kdBZcTJ7tL2MMiNWfA) (NI hFindFile_p0, tyObject_WIN32_FIND_DATA__U9aHFJYS6OptpDrgTrpKXbQ* lpFindFileData_p1);
typedef N_STDCALL_PTR(NI, tyProc__zP2zGemcuVRvOUE82f9a0Pw) (NI32 nStdHandle_p0);
typedef N_STDCALL_PTR(NI32, tyProc__G0MNqLrkAnRVz4cDlgVcRg) (NI handle_p0, NI16* buf_p1, NI32 size_p2);
typedef N_STDCALL_PTR(NI32, tyProc__5sqIMptsyAfPn3c9cPj4DOA) (NI16* lpFileName_p0);
typedef N_STDCALL_PTR(NI32, tyProc__Rzv0SS9bu3vYSxhvPQEKMBQ) (NI16* lpFileName_p0, NI32 nBufferLength_p1, NI16* lpBuffer_p2, NI16** lpFilePart_p3);
typedef N_STDCALL_PTR(NI32, tyProc__uBkkHOkdpyfFusi9cbg9cHww) (NI16* pathName_p0, void* security_p1);
typedef N_STDCALL_PTR(NI32, tyProc__aOhJuXP2rSFzW5eC9bSzvwg) (NI16* lpFileName_p0, NI32 dwFileAttributes_p1);
typedef N_STDCALL_PTR(NI32, tyProc__oy7Ihkf1tW3Sy3LTPmCMBg) (NI* hReadPipe_p0, NI* hWritePipe_p1, tyObject_SECURITY_ATTRIBUTES__uDMqUzOPuH1lv6sAMGbeUQ* lpPipeAttributes_p2, NI32 nSize_p3);
typedef N_STDCALL_PTR(NI32, tyProc__mBVOPXmpGfUrrbA9a5K9cMbQ) (NI hObject_p0, NI32 dwMask_p1, NI32 dwFlags_p2);
typedef N_STDCALL_PTR(NI, tyProc__8z1SB6tB3wQ9chvtRg7okWg) (NI16* lpName_p0, NI32 dwOpenMode_p1, NI32 dwPipeMode_p2, NI32 nMaxInstances_p3, NI32 nOutBufferSize_p4, NI32 nInBufferSize_p5, NI32 nDefaultTimeOut_p6, tyObject_SECURITY_ATTRIBUTES__uDMqUzOPuH1lv6sAMGbeUQ* lpSecurityAttributes_p7);
typedef N_STDCALL_PTR(NI, tyProc__Q9box9as0S1hHKH9cSO3MGDDA) (NI16* lpFileName_p0, NI32 dwDesiredAccess_p1, NI32 dwShareMode_p2, void* lpSecurityAttributes_p3, NI32 dwCreationDisposition_p4, NI32 dwFlagsAndAttributes_p5, NI hTemplateFile_p6);
typedef N_STDCALL_PTR(NI, tyProc__uryidYWkntM7ddjZSyxvyQ) (void);
typedef N_STDCALL_PTR(NI32, tyProc__oew1DfaGl5XhHmnBK0HYXQ) (NI hSourceProcessHandle_p0, NI hSourceHandle_p1, NI hTargetProcessHandle_p2, NI* lpTargetHandle_p3, NI32 dwDesiredAccess_p4, NI32 bInheritHandle_p5, NI32 dwOptions_p6);
typedef N_STDCALL_PTR(NI32, tyProc__P13srMBg9b3d3yEV9aW4NCoA) (NI hObject_p0);
typedef N_STDCALL_PTR(NI32, tyProc__ZTTeZhY06cLyG9cYM2AMnJA) (NI16* lpApplicationName_p0, NI16* lpCommandLine_p1, tyObject_SECURITY_ATTRIBUTES__uDMqUzOPuH1lv6sAMGbeUQ* lpProcessAttributes_p2, tyObject_SECURITY_ATTRIBUTES__uDMqUzOPuH1lv6sAMGbeUQ* lpThreadAttributes_p3, NI32 bInheritHandles_p4, NI32 dwCreationFlags_p5, NI16* lpEnvironment_p6, NI16* lpCurrentDirectory_p7, tyObject_STARTUPINFO__WgdDLv9bNl1jYz9blfSJnoyw* lpStartupInfo_p8, tyObject_PROCESS_INFORMATION__rtX62MtURAW2J5XCXdN9c3g* lpProcessInformation_p9);
typedef N_STDCALL_PTR(NI32, tyProc__9ajZYMGL5GqiRFqiCC367oA) (NI hFile_p0, void* buffer_p1, NI32 nNumberOfBytesToRead_p2, NI32* lpNumberOfBytesRead_p3, void* lpOverlapped_p4);
typedef N_STDCALL_PTR(NI32, tyProc__FprzxJFfqQdWyyGxiUsmeQ) (NI hHandle_p0, NI32 dwMilliseconds_p1);
typedef N_STDCALL_PTR(NI32, tyProc__7S2vVNF4dlY59blFohNWnHg) (NI hProcess_p0, NI uExitCode_p1);
typedef N_STDCALL_PTR(NI32, tyProc__KxeGXN001TReCr89ca9c5bng) (NI hProcess_p0, NI32* lpExitCode_p1);
typedef N_STDCALL_PTR(void, tyProc__aUSMVdxC4gPInbVUapv4qQ) (tyObject_FILETIME__tx5ctSQ9aLxkzr39cl7M9cXrQ* lpSystemTimeAsFileTime_p0);
struct tyObject_FILETIME__tx5ctSQ9aLxkzr39cl7M9cXrQ {
	NI32 dwLowDateTime;
	NI32 dwHighDateTime;
};
typedef N_STDCALL_PTR(NI32, tyProc__29cQilB9cqahn5yPQZH66TkA) (NI16* lpExistingFileName_p0, NI16* lpNewFileName_p1, NI32 bFailIfExists_p2);
typedef N_STDCALL_PTR(NI32, tyProc__Dx0emMHXXGxkRnYGUaFLbg) (NI32 nCount_p0, NI* lpHandles_p1, NI32 bWaitAll_p2, NI32 dwMilliseconds_p3);
typedef N_STDCALL_PTR(NI, tyProc__UI8KCxlXQHWT6J0VRUWTmA) (NI hwnd_p0, NI16* lpOperation_p1, NI16* lpFile_p2, NI16* lpParameters_p3, NI16* lpDirectory_p4, NI32 nShowCmd_p5);
N_LIB_PRIVATE N_NIMCALL(void*, loadLib__pureZdynlib_u3)(NimStringV2 path_p0, NIM_BOOL globalSymbols_p1);
N_LIB_PRIVATE N_NIMCALL(void*, symAddr__pureZdynlib_u74)(void* lib_p0, NCSTRING name_p1);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
N_LIB_PRIVATE N_NIMCALL(void*, nimLoadLibrary)(NimStringV2 path_p0);
N_LIB_PRIVATE N_NIMCALL(void, nimLoadLibraryError)(NimStringV2 path_p0);
N_LIB_PRIVATE N_NIMCALL(void*, nimGetProcAddr)(void* lib_p0, NCSTRING name_p1);
N_LIB_PRIVATE N_NIMCALL(void, nimTestErrorFlag)(void);
static const struct {
  NI cap; NIM_CHAR data[10+1];
} TM__k6kyf4Co79a84IkK9blFuQVA_2 = { 10 | NIM_STRLIT_FLAG, "Ws2_32.dll" };
static const NimStringV2 TM__k6kyf4Co79a84IkK9blFuQVA_3 = {10, (NimStrPayload*)&TM__k6kyf4Co79a84IkK9blFuQVA_2};
static const struct {
  NI cap; NIM_CHAR data[8+1];
} TM__k6kyf4Co79a84IkK9blFuQVA_6 = { 8 | NIM_STRLIT_FLAG, "kernel32" };
static const NimStringV2 TM__k6kyf4Co79a84IkK9blFuQVA_7 = {8, (NimStrPayload*)&TM__k6kyf4Co79a84IkK9blFuQVA_6};
static const struct {
  NI cap; NIM_CHAR data[8+1];
} TM__k6kyf4Co79a84IkK9blFuQVA_8 = { 8 | NIM_STRLIT_FLAG, "kernel32" };
static const NimStringV2 TM__k6kyf4Co79a84IkK9blFuQVA_9 = {8, (NimStrPayload*)&TM__k6kyf4Co79a84IkK9blFuQVA_8};
static const struct {
  NI cap; NIM_CHAR data[11+1];
} TM__k6kyf4Co79a84IkK9blFuQVA_84 = { 11 | NIM_STRLIT_FLAG, "shell32.dll" };
static const NimStringV2 TM__k6kyf4Co79a84IkK9blFuQVA_85 = {11, (NimStrPayload*)&TM__k6kyf4Co79a84IkK9blFuQVA_84};
static const struct {
  NI cap; NIM_CHAR data[11+1];
} TM__k6kyf4Co79a84IkK9blFuQVA_86 = { 11 | NIM_STRLIT_FLAG, "shell32.dll" };
static const NimStringV2 TM__k6kyf4Co79a84IkK9blFuQVA_87 = {11, (NimStrPayload*)&TM__k6kyf4Co79a84IkK9blFuQVA_86};
N_LIB_PRIVATE tyObject_GUID__KhSF19anzUEjuSNqxodiVvQ WSAID_CONNECTEX__windowsZwinlean_u725 = {((NI32)631375801), ((NI16)-8717), ((NI16)18016), {((NI8)-114),
((NI8)-23),
((NI8)118),
((NI8)-27),
((NI8)-116),
((NI8)116),
((NI8)6),
((NI8)62)}
}
;
N_LIB_PRIVATE tyObject_GUID__KhSF19anzUEjuSNqxodiVvQ WSAID_ACCEPTEX__windowsZwinlean_u726 = {((NI32)-1254720015), ((NI16)-13396), ((NI16)4559), {((NI8)-107),
((NI8)-54),
((NI8)0),
((NI8)-128),
((NI8)95),
((NI8)72),
((NI8)-95),
((NI8)-110)}
}
;
N_LIB_PRIVATE tyObject_GUID__KhSF19anzUEjuSNqxodiVvQ WSAID_GETACCEPTEXSOCKADDRS__windowsZwinlean_u727 = {((NI32)-1254720014), ((NI16)-13396), ((NI16)4559), {((NI8)-107),
((NI8)-54),
((NI8)0),
((NI8)-128),
((NI8)95),
((NI8)72),
((NI8)-95),
((NI8)-110)}
}
;
N_LIB_PRIVATE tyProc__7LMcD5Rujqk7H2JqdJtoyA inet_ntop_real__windowsZwinlean_u798 = NIM_NIL;
N_LIB_PRIVATE void* ws2__windowsZwinlean_u799;
extern NIM_BOOL nimInErrorMode__system_u4217;
static void* TM__k6kyf4Co79a84IkK9blFuQVA_4;
tyProc__9bXHKfH08cKXphxNi5GYejg Dl_1476395291_;
tyProc__aMOow6Lp4fo8dW2ko9aU3pg Dl_1476395192_;
tyProc__aO673xGTLLxou7P7GxoCXA Dl_1476395182_;
tyProc__im9buRnIvptJfzdASYMEbBA Dl_1476395190_;
tyProc__9bXer9a4ps9aSGctILcxWReVw Dl_1476395179_;
tyProc__mLqKqb2iQ7G5ICPX33ZfZg Dl_1476395259_;
tyProc__ofoySXaAAlxxs9bQS9a1etlg Dl_1476395265_;
tyProc__h9aq7kdBZcTJ7tL2MMiNWfA Dl_1476395262_;
tyProc__zP2zGemcuVRvOUE82f9a0Pw Dl_1476395172_;
tyProc__G0MNqLrkAnRVz4cDlgVcRg Dl_1476395205_;
tyProc__5sqIMptsyAfPn3c9cPj4DOA Dl_1476395272_;
tyProc__Rzv0SS9bu3vYSxhvPQEKMBQ Dl_1476395267_;
tyProc__uBkkHOkdpyfFusi9cbg9cHww Dl_1476395197_;
tyProc__5sqIMptsyAfPn3c9cPj4DOA Dl_1476395627_;
tyProc__aOhJuXP2rSFzW5eC9bSzvwg Dl_1476395274_;
tyProc__oy7Ihkf1tW3Sy3LTPmCMBg Dl_1476395127_;
tyProc__mBVOPXmpGfUrrbA9a5K9cMbQ Dl_1476395614_;
tyProc__8z1SB6tB3wQ9chvtRg7okWg Dl_1476395132_;
tyProc__Q9box9as0S1hHKH9cSO3MGDDA Dl_1476395619_;
tyProc__uryidYWkntM7ddjZSyxvyQ Dl_1476395618_;
tyProc__oew1DfaGl5XhHmnBK0HYXQ Dl_1476395603_;
tyProc__P13srMBg9b3d3yEV9aW4NCoA Dl_1476395113_;
tyProc__ZTTeZhY06cLyG9cYM2AMnJA Dl_1476395148_;
tyProc__9ajZYMGL5GqiRFqiCC367oA Dl_1476395115_;
tyProc__9ajZYMGL5GqiRFqiCC367oA Dl_1476395121_;
tyProc__FprzxJFfqQdWyyGxiUsmeQ Dl_1476395163_;
tyProc__7S2vVNF4dlY59blFohNWnHg Dl_1476395166_;
tyProc__KxeGXN001TReCr89ca9c5bng Dl_1476395169_;
tyProc__aUSMVdxC4gPInbVUapv4qQ Dl_1476395306_;
tyProc__9bXHKfH08cKXphxNi5GYejg Dl_1476395288_;
tyProc__5sqIMptsyAfPn3c9cPj4DOA Dl_1476395289_;
tyProc__29cQilB9cqahn5yPQZH66TkA Dl_1476395277_;
tyProc__5sqIMptsyAfPn3c9cPj4DOA Dl_1476395200_;
tyProc__5sqIMptsyAfPn3c9cPj4DOA Dl_1476395195_;
tyProc__29cQilB9cqahn5yPQZH66TkA Dl_1476395284_;
tyProc__Dx0emMHXXGxkRnYGUaFLbg Dl_1476395565_;
static void* TM__k6kyf4Co79a84IkK9blFuQVA_82;
tyProc__UI8KCxlXQHWT6J0VRUWTmA Dl_1476395310_;
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4217);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NI64, rdFileTime__windowsZwinlean_u284)(tyObject_FILETIME__tx5ctSQ9aLxkzr39cl7M9cXrQ f_p0) {
	NI64 result;
	result = (NI64)(((NI64) (((NU32) (f_p0.dwLowDateTime)))) | (NI64)((NU64)(((NI64) (((NU32) (f_p0.dwHighDateTime))))) << (NU64)(((NI)32))));
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, atmdotdotatslibatswindowsatswinleandotnim_Init000)(void) {
{
	void* T1_;
NIM_BOOL* nimErr_;
nimErr_ = nimErrorFlag();
	T1_ = (void*)0;
	T1_ = loadLib__pureZdynlib_u3(TM__k6kyf4Co79a84IkK9blFuQVA_3, NIM_FALSE);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	ws2__windowsZwinlean_u799 = T1_;
	{
		void* T6_;
		if (!!((ws2__windowsZwinlean_u799 == NIM_NIL))) goto LA4_;
		T6_ = (void*)0;
		T6_ = symAddr__pureZdynlib_u74(ws2__windowsZwinlean_u799, "inet_ntop");
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		inet_ntop_real__windowsZwinlean_u798 = ((tyProc__7LMcD5Rujqk7H2JqdJtoyA) (T6_));
	}
LA4_: ;
	BeforeRet_: ;
	nimTestErrorFlag();
}
}

N_LIB_PRIVATE N_NIMCALL(void, atmdotdotatslibatswindowsatswinleandotnim_DatInit000)(void) {
if (!((TM__k6kyf4Co79a84IkK9blFuQVA_4 = nimLoadLibrary(TM__k6kyf4Co79a84IkK9blFuQVA_7))
)) nimLoadLibraryError(TM__k6kyf4Co79a84IkK9blFuQVA_9);
	Dl_1476395291_ = (tyProc__9bXHKfH08cKXphxNi5GYejg) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "GetCommandLineW");
	Dl_1476395192_ = (tyProc__aMOow6Lp4fo8dW2ko9aU3pg) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "GetCurrentDirectoryW");
	Dl_1476395182_ = (tyProc__aO673xGTLLxou7P7GxoCXA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "FormatMessageW");
	Dl_1476395190_ = (tyProc__im9buRnIvptJfzdASYMEbBA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "LocalFree");
	Dl_1476395179_ = (tyProc__9bXer9a4ps9aSGctILcxWReVw) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "GetLastError");
	Dl_1476395259_ = (tyProc__mLqKqb2iQ7G5ICPX33ZfZg) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "FindFirstFileW");
	Dl_1476395265_ = (tyProc__ofoySXaAAlxxs9bQS9a1etlg) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "FindClose");
	Dl_1476395262_ = (tyProc__h9aq7kdBZcTJ7tL2MMiNWfA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "FindNextFileW");
	Dl_1476395172_ = (tyProc__zP2zGemcuVRvOUE82f9a0Pw) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "GetStdHandle");
	Dl_1476395205_ = (tyProc__G0MNqLrkAnRVz4cDlgVcRg) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "GetModuleFileNameW");
	Dl_1476395272_ = (tyProc__5sqIMptsyAfPn3c9cPj4DOA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "GetFileAttributesW");
	Dl_1476395267_ = (tyProc__Rzv0SS9bu3vYSxhvPQEKMBQ) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "GetFullPathNameW");
	Dl_1476395197_ = (tyProc__uBkkHOkdpyfFusi9cbg9cHww) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "CreateDirectoryW");
	Dl_1476395627_ = (tyProc__5sqIMptsyAfPn3c9cPj4DOA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "DeleteFileW");
	Dl_1476395274_ = (tyProc__aOhJuXP2rSFzW5eC9bSzvwg) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "SetFileAttributesW");
	Dl_1476395127_ = (tyProc__oy7Ihkf1tW3Sy3LTPmCMBg) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "CreatePipe");
	Dl_1476395614_ = (tyProc__mBVOPXmpGfUrrbA9a5K9cMbQ) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "SetHandleInformation");
	Dl_1476395132_ = (tyProc__8z1SB6tB3wQ9chvtRg7okWg) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "CreateNamedPipeW");
	Dl_1476395619_ = (tyProc__Q9box9as0S1hHKH9cSO3MGDDA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "CreateFileW");
	Dl_1476395618_ = (tyProc__uryidYWkntM7ddjZSyxvyQ) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "GetCurrentProcess");
	Dl_1476395603_ = (tyProc__oew1DfaGl5XhHmnBK0HYXQ) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "DuplicateHandle");
	Dl_1476395113_ = (tyProc__P13srMBg9b3d3yEV9aW4NCoA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "CloseHandle");
	Dl_1476395148_ = (tyProc__ZTTeZhY06cLyG9cYM2AMnJA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "CreateProcessW");
	Dl_1476395115_ = (tyProc__9ajZYMGL5GqiRFqiCC367oA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "ReadFile");
	Dl_1476395121_ = (tyProc__9ajZYMGL5GqiRFqiCC367oA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "WriteFile");
	Dl_1476395163_ = (tyProc__FprzxJFfqQdWyyGxiUsmeQ) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "WaitForSingleObject");
	Dl_1476395166_ = (tyProc__7S2vVNF4dlY59blFohNWnHg) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "TerminateProcess");
	Dl_1476395169_ = (tyProc__KxeGXN001TReCr89ca9c5bng) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "GetExitCodeProcess");
	Dl_1476395306_ = (tyProc__aUSMVdxC4gPInbVUapv4qQ) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "GetSystemTimeAsFileTime");
	Dl_1476395288_ = (tyProc__9bXHKfH08cKXphxNi5GYejg) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "GetEnvironmentStringsW");
	Dl_1476395289_ = (tyProc__5sqIMptsyAfPn3c9cPj4DOA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "FreeEnvironmentStringsW");
	Dl_1476395277_ = (tyProc__29cQilB9cqahn5yPQZH66TkA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "CopyFileW");
	Dl_1476395200_ = (tyProc__5sqIMptsyAfPn3c9cPj4DOA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "RemoveDirectoryW");
	Dl_1476395195_ = (tyProc__5sqIMptsyAfPn3c9cPj4DOA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "SetCurrentDirectoryW");
	Dl_1476395284_ = (tyProc__29cQilB9cqahn5yPQZH66TkA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "MoveFileExW");
	Dl_1476395565_ = (tyProc__Dx0emMHXXGxkRnYGUaFLbg) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_4, "WaitForMultipleObjects");
if (!((TM__k6kyf4Co79a84IkK9blFuQVA_82 = nimLoadLibrary(TM__k6kyf4Co79a84IkK9blFuQVA_85))
)) nimLoadLibraryError(TM__k6kyf4Co79a84IkK9blFuQVA_87);
	Dl_1476395310_ = (tyProc__UI8KCxlXQHWT6J0VRUWTmA) nimGetProcAddr(TM__k6kyf4Co79a84IkK9blFuQVA_82, "ShellExecuteW");
}

