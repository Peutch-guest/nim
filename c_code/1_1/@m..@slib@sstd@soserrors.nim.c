/* Generated by Nim Compiler v2.0.8 */
#define NIM_INTBITS 32

#include "nimbase.h"
#include <string.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct tyObject_OSError__wbdVnEIpPL9bYHx1qGDWWqg tyObject_OSError__wbdVnEIpPL9bYHx1qGDWWqg;
typedef struct tyObject_CatchableError__ZTYPviQZG30lfpIxrkxMVg tyObject_CatchableError__ZTYPviQZG30lfpIxrkxMVg;
typedef struct Exception Exception;
typedef struct RootObj RootObj;
typedef struct TNimTypeV2 TNimTypeV2;
typedef struct tySequence__XN9c0R4P9a1Nf5KDUornfebA tySequence__XN9c0R4P9a1Nf5KDUornfebA;
typedef struct tySequence__XN9c0R4P9a1Nf5KDUornfebA_Content tySequence__XN9c0R4P9a1Nf5KDUornfebA_Content;
typedef struct tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA;
typedef struct tyObject_StackTraceEntry__9bkrelJjTRvP2jITkMvCajw tyObject_StackTraceEntry__9bkrelJjTRvP2jITkMvCajw;
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
struct TNimTypeV2 {
	void* destructor;
	NI size;
	NI16 align;
	NI16 depth;
	NU32* display;
	void* traceImpl;
	void* typeInfoV1;
	NI flags;
};
struct RootObj {
TNimTypeV2* m_type;
};
struct tySequence__XN9c0R4P9a1Nf5KDUornfebA {
  NI len; tySequence__XN9c0R4P9a1Nf5KDUornfebA_Content* p;
};
struct Exception {
  RootObj Sup;
	Exception* parent;
	NCSTRING name;
	NimStringV2 message;
	tySequence__XN9c0R4P9a1Nf5KDUornfebA trace;
	Exception* up;
};
struct tyObject_CatchableError__ZTYPviQZG30lfpIxrkxMVg {
  Exception Sup;
};
struct tyObject_OSError__wbdVnEIpPL9bYHx1qGDWWqg {
  tyObject_CatchableError__ZTYPviQZG30lfpIxrkxMVg Sup;
	NI32 errorCode;
};
typedef N_STDCALL_PTR(NI32, tyProc__aO673xGTLLxou7P7GxoCXA) (NI32 dwFlags_p0, void* lpSource_p1, NI32 dwMessageId_p2, NI32 dwLanguageId_p3, void* lpBuffer_p4, NI32 nSize_p5, void* arguments_p6);
typedef N_STDCALL_PTR(void, tyProc__im9buRnIvptJfzdASYMEbBA) (void* p_p0);
typedef N_STDCALL_PTR(NI32, tyProc__9bXer9a4ps9aSGctILcxWReVw) (void);
struct tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA {
	NI rc;
	NI rootIdx;
};
struct tyObject_StackTraceEntry__9bkrelJjTRvP2jITkMvCajw {
	NCSTRING procname;
	NI line;
	NCSTRING filename;
};
struct tySequence__XN9c0R4P9a1Nf5KDUornfebA_Content { NI cap; tyObject_StackTraceEntry__9bkrelJjTRvP2jITkMvCajw data[SEQ_DECL_SIZE]; };
N_LIB_PRIVATE N_NOINLINE(tyObject_OSError__wbdVnEIpPL9bYHx1qGDWWqg*, newOSError__stdZoserrors_u57)(NI32 errorCode_p0, NimStringV2 additionalInfo_p1);
N_LIB_PRIVATE N_NIMCALL(void*, nimNewObj)(NI size_p0, NI alignment_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, osErrorMsg__stdZoserrors_u12)(NI32 errorCode_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqsink___stdZassertions_u22)(NimStringV2* dest_p0, NimStringV2 src_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, dollar___stdZwidestrs_u385)(NI16* s_p0);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
static N_INLINE(NIM_CHAR*, X5BX5D___system_u7801)(NimStringV2* s_p0, NI i_p1);
static N_INLINE(void, nimPrepareStrMutationV2)(NimStringV2* s_p0);
N_LIB_PRIVATE N_NIMCALL(void, nimPrepareStrMutationImpl__system_u2390)(NimStringV2* s_p0);
static N_INLINE(void, nimAddCharV1)(NimStringV2* s_p0, NIM_CHAR c_p1);
N_LIB_PRIVATE N_NIMCALL(void, prepareAdd)(NimStringV2* s_p0, NI addLen_p1);
static N_INLINE(void, appendString)(NimStringV2* dest_p0, NimStringV2 src_p1);
static N_INLINE(void, copyMem__system_u1740)(void* dest_p0, void* source_p1, NI size_p2);
static N_INLINE(void, nimCopyMem)(void* dest_p0, void* source_p1, NI size_p2);
N_LIB_PRIVATE N_NIMCALL(void, raiseExceptionEx)(Exception* e_p0, NCSTRING ename_p1, NCSTRING procname_p2, NCSTRING filename_p3, NI line_p4);
static N_INLINE(NIM_BOOL, nimDecRefIsLastCyclicDyn)(void* p_p0);
static N_INLINE(NI, minuspercent___system_u810)(NI x_p0, NI y_p1);
N_LIB_PRIVATE N_NOINLINE(void, rememberCycle__system_u3429)(NIM_BOOL isDestroyAction_p0, tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA* s_p1, TNimTypeV2* desc_p2);
N_LIB_PRIVATE N_NIMCALL(void, nimDestroyAndDispose)(void* p_p0);
N_LIB_PRIVATE N_NIMCALL(void, nimTestErrorFlag)(void);
extern TNimTypeV2 NTIv2__wbdVnEIpPL9bYHx1qGDWWqg_;
static const struct {
  NI cap; NIM_CHAR data[0+1];
} TM__1nRK0qvLEFLABF9aZnNtAXQ_2 = { 0 | NIM_STRLIT_FLAG, "" };
static const NimStringV2 TM__1nRK0qvLEFLABF9aZnNtAXQ_3 = {0, (NimStrPayload*)&TM__1nRK0qvLEFLABF9aZnNtAXQ_2};
static const struct {
  NI cap; NIM_CHAR data[17+1];
} TM__1nRK0qvLEFLABF9aZnNtAXQ_4 = { 17 | NIM_STRLIT_FLAG, "Additional info: " };
static const NimStringV2 TM__1nRK0qvLEFLABF9aZnNtAXQ_5 = {17, (NimStrPayload*)&TM__1nRK0qvLEFLABF9aZnNtAXQ_4};
static const struct {
  NI cap; NIM_CHAR data[16+1];
} TM__1nRK0qvLEFLABF9aZnNtAXQ_6 = { 16 | NIM_STRLIT_FLAG, "unknown OS error" };
static const NimStringV2 TM__1nRK0qvLEFLABF9aZnNtAXQ_7 = {16, (NimStrPayload*)&TM__1nRK0qvLEFLABF9aZnNtAXQ_6};
extern tyProc__aO673xGTLLxou7P7GxoCXA Dl_1476395182_;
extern tyProc__im9buRnIvptJfzdASYMEbBA Dl_1476395190_;
extern NIM_BOOL nimInErrorMode__system_u4217;
extern tyProc__9bXer9a4ps9aSGctILcxWReVw Dl_1476395179_;
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4217);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, osErrorMsg__stdZoserrors_u12)(NI32 errorCode_p0) {
	NimStringV2 result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = TM__1nRK0qvLEFLABF9aZnNtAXQ_3;
	{
		NI16* msgbuf;
		if (!!((errorCode_p0 == ((NI32)0)))) goto LA3_;
		msgbuf = (NI16*)0;
		{
			NI32 T7_;
			NimStringV2 T10_;
			T7_ = (NI32)0;
			T7_ = Dl_1476395182_(((NI32)4864), NIM_NIL, errorCode_p0, ((NI32)0), ((void*) (&msgbuf)), ((NI32)0), NIM_NIL);
			if (!!((T7_ == ((NI32)0)))) goto LA8_;
			T10_.len = 0; T10_.p = NIM_NIL;
			T10_ = dollar___stdZwidestrs_u385(msgbuf);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			eqsink___stdZassertions_u22((&result), T10_);
			{
				if (!!((msgbuf == ((NI16*) NIM_NIL)))) goto LA13_;
				Dl_1476395190_(((void*) (msgbuf)));
			}
LA13_: ;
		}
LA8_: ;
	}
LA3_: ;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(void, nimPrepareStrMutationV2)(NimStringV2* s_p0) {
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = !(((*s_p0).p == ((NimStrPayload*) NIM_NIL)));
		if (!(T3_)) goto LA4_;
		T3_ = ((NI)((*(*s_p0).p).cap & ((NI)1073741824)) == ((NI)1073741824));
LA4_: ;
		if (!T3_) goto LA5_;
		nimPrepareStrMutationImpl__system_u2390(s_p0);
	}
LA5_: ;
}
static N_INLINE(NIM_CHAR*, X5BX5D___system_u7801)(NimStringV2* s_p0, NI i_p1) {
	NIM_CHAR* result;
	nimPrepareStrMutationV2((&(*s_p0)));
	result = (&(*s_p0).p->data[(NI)((*s_p0).len - i_p1)]);
	return result;
}
static N_INLINE(void, nimAddCharV1)(NimStringV2* s_p0, NIM_CHAR c_p1) {
	prepareAdd(s_p0, ((NI)1));
	(*(*s_p0).p).data[(*s_p0).len] = c_p1;
	(*s_p0).len += ((NI)1);
	(*(*s_p0).p).data[(*s_p0).len] = 0;
}
static N_INLINE(void, nimCopyMem)(void* dest_p0, void* source_p1, NI size_p2) {
	void* T1_;
	T1_ = (void*)0;
	T1_ = memcpy(dest_p0, source_p1, ((size_t) (size_p2)));
}
static N_INLINE(void, copyMem__system_u1740)(void* dest_p0, void* source_p1, NI size_p2) {
	nimCopyMem(dest_p0, source_p1, size_p2);
}
static N_INLINE(void, appendString)(NimStringV2* dest_p0, NimStringV2 src_p1) {
	{
		if (!(((NI)0) < src_p1.len)) goto LA3_;
		copyMem__system_u1740(((void*) ((&(*(*dest_p0).p).data[(*dest_p0).len]))), ((void*) ((&(*src_p1.p).data[((NI)0)]))), ((NI) ((NI)(src_p1.len + ((NI)1)))));
		(*dest_p0).len += src_p1.len;
	}
LA3_: ;
}
N_LIB_PRIVATE N_NOINLINE(tyObject_OSError__wbdVnEIpPL9bYHx1qGDWWqg*, newOSError__stdZoserrors_u57)(NI32 errorCode_p0, NimStringV2 additionalInfo_p1) {
	tyObject_OSError__wbdVnEIpPL9bYHx1qGDWWqg* result;
	NI32 colontmpD_;
	tyObject_OSError__wbdVnEIpPL9bYHx1qGDWWqg* T1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	colontmpD_ = (NI32)0;
	T1_ = NIM_NIL;
	T1_ = (tyObject_OSError__wbdVnEIpPL9bYHx1qGDWWqg*) nimNewObj(sizeof(tyObject_OSError__wbdVnEIpPL9bYHx1qGDWWqg), NIM_ALIGNOF(tyObject_OSError__wbdVnEIpPL9bYHx1qGDWWqg));
	(*T1_).Sup.Sup.Sup.m_type = (&NTIv2__wbdVnEIpPL9bYHx1qGDWWqg_);
	(*T1_).Sup.Sup.name = "OSError";
	colontmpD_ = errorCode_p0;
	(*T1_).errorCode = colontmpD_;
	(*T1_).Sup.Sup.message = osErrorMsg__stdZoserrors_u12(errorCode_p0);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	result = T1_;
	{
		if (!(((NI)0) < additionalInfo_p1.len)) goto LA4_;
		{
			NIM_BOOL T8_;
			NIM_CHAR* T10_;
			T8_ = (NIM_BOOL)0;
			T8_ = (((NI)0) < (*result).Sup.Sup.message.len);
			if (!(T8_)) goto LA9_;
			T10_ = (NIM_CHAR*)0;
			T10_ = X5BX5D___system_u7801((&(*result).Sup.Sup.message), ((NI)1));
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			T8_ = !(((NU8)((*T10_)) == (NU8)(10)));
LA9_: ;
			if (!T8_) goto LA11_;
			nimAddCharV1((&(*result).Sup.Sup.message), 10);
		}
LA11_: ;
		prepareAdd((&(*result).Sup.Sup.message), 17);
appendString((&(*result).Sup.Sup.message), TM__1nRK0qvLEFLABF9aZnNtAXQ_5);
		prepareAdd((&(*result).Sup.Sup.message), additionalInfo_p1.len + 0);
appendString((&(*result).Sup.Sup.message), additionalInfo_p1);
	}
LA4_: ;
	{
		if (!((*result).Sup.Sup.message.len == 0)) goto LA15_;
		eqsink___stdZassertions_u22((&(*result).Sup.Sup.message), TM__1nRK0qvLEFLABF9aZnNtAXQ_7);
	}
LA15_: ;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NOINLINE(void, raiseOSError__stdZoserrors_u123)(NI32 errorCode_p0, NimStringV2 additionalInfo_p1) {
	tyObject_OSError__wbdVnEIpPL9bYHx1qGDWWqg* T1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	T1_ = NIM_NIL;
	T1_ = newOSError__stdZoserrors_u57(errorCode_p0, additionalInfo_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	raiseExceptionEx((Exception*)T1_, "OSError", "raiseOSError", "oserrors.nim", 92);
goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(NI32, osLastError__stdZoserrors_u126)(void) {
	NI32 result;
	NI32 T1_;
	T1_ = (NI32)0;
	T1_ = Dl_1476395179_();
	result = ((NI32) (T1_));
	return result;
}
static N_INLINE(NI, minuspercent___system_u810)(NI x_p0, NI y_p1) {
	NI result;
	result = ((NI) ((NU)((NU32)(((NU) (x_p0))) - (NU32)(((NU) (y_p1))))));
	return result;
}
static N_INLINE(NIM_BOOL, nimDecRefIsLastCyclicDyn)(void* p_p0) {
	NIM_BOOL result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	{
		tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA* cell;
		NI T5_;
		if (!!((p_p0 == NIM_NIL))) goto LA3_;
		T5_ = (NI)0;
		T5_ = minuspercent___system_u810(((NI) (ptrdiff_t) (p_p0)), ((NI)8));
		cell = ((tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA*) (T5_));
		{
			if (!((NI)((*cell).rc & ((NI)-16)) == ((NI)0))) goto LA8_;
			result = NIM_TRUE;
		}
		goto LA6_;
LA8_: ;
		{
			(*cell).rc -= ((NI)16);
		}
LA6_: ;
		rememberCycle__system_u3429(result, cell, (*((TNimTypeV2**) (p_p0))));
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
LA3_: ;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___stdZoserrors_u102)(tyObject_OSError__wbdVnEIpPL9bYHx1qGDWWqg* dest_p0) {
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = nimDecRefIsLastCyclicDyn(dest_p0);
		if (!T3_) goto LA4_;
		nimDestroyAndDispose(dest_p0);
	}
LA4_: ;
}
