/* Generated by Nim Compiler v2.0.8 */
#define NIM_INTBITS 32
#define NIM_EmulateOverflowChecks

#include "nimbase.h"
#include <string.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct tyObject_IndexDefect__rICUfwx5lYXxZ9c1cjKQIxQ tyObject_IndexDefect__rICUfwx5lYXxZ9c1cjKQIxQ;
typedef struct tyObject_Defect__9cvzu9c5YKtjp737ZrwCt4wg tyObject_Defect__9cvzu9c5YKtjp737ZrwCt4wg;
typedef struct Exception Exception;
typedef struct RootObj RootObj;
typedef struct TNimTypeV2 TNimTypeV2;
typedef struct tySequence__XN9c0R4P9a1Nf5KDUornfebA tySequence__XN9c0R4P9a1Nf5KDUornfebA;
typedef struct tySequence__XN9c0R4P9a1Nf5KDUornfebA_Content tySequence__XN9c0R4P9a1Nf5KDUornfebA_Content;
typedef struct tySequence__sM4lkSb7zS6F7OVMvW9cffQ tySequence__sM4lkSb7zS6F7OVMvW9cffQ;
typedef struct tySequence__sM4lkSb7zS6F7OVMvW9cffQ_Content tySequence__sM4lkSb7zS6F7OVMvW9cffQ_Content;
typedef struct tyObject_StackTraceEntry__9bkrelJjTRvP2jITkMvCajw tyObject_StackTraceEntry__9bkrelJjTRvP2jITkMvCajw;
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
struct TNimTypeV2 {
	void* destructor;
	NI size;
	NI16 align;
	NI16 depth;
	NU32* display;
	void* traceImpl;
	void* typeInfoV1;
	NI flags;
};
struct RootObj {
TNimTypeV2* m_type;
};
struct tySequence__XN9c0R4P9a1Nf5KDUornfebA {
  NI len; tySequence__XN9c0R4P9a1Nf5KDUornfebA_Content* p;
};
struct Exception {
  RootObj Sup;
	Exception* parent;
	NCSTRING name;
	NimStringV2 message;
	tySequence__XN9c0R4P9a1Nf5KDUornfebA trace;
	Exception* up;
};
struct tyObject_Defect__9cvzu9c5YKtjp737ZrwCt4wg {
  Exception Sup;
};
struct tyObject_IndexDefect__rICUfwx5lYXxZ9c1cjKQIxQ {
  tyObject_Defect__9cvzu9c5YKtjp737ZrwCt4wg Sup;
};
struct tySequence__sM4lkSb7zS6F7OVMvW9cffQ {
  NI len; tySequence__sM4lkSb7zS6F7OVMvW9cffQ_Content* p;
};
struct tyObject_StackTraceEntry__9bkrelJjTRvP2jITkMvCajw {
	NCSTRING procname;
	NI line;
	NCSTRING filename;
};
struct tySequence__XN9c0R4P9a1Nf5KDUornfebA_Content { NI cap; tyObject_StackTraceEntry__9bkrelJjTRvP2jITkMvCajw data[SEQ_DECL_SIZE]; };
struct tySequence__sM4lkSb7zS6F7OVMvW9cffQ_Content { NI cap; NimStringV2 data[SEQ_DECL_SIZE]; };
N_LIB_PRIVATE N_NIMCALL(NimStringV2, cstrToNimstr)(NCSTRING str_p0);
N_LIB_PRIVATE N_NIMCALL(void*, nimNewObj)(NI size_p0, NI alignment_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___stdZassertions_u13)(NimStringV2* dest_p0);
static N_INLINE(void, appendString)(NimStringV2* dest_p0, NimStringV2 src_p1);
static N_INLINE(void, copyMem__system_u1740)(void* dest_p0, void* source_p1, NI size_p2);
static N_INLINE(void, nimCopyMem)(void* dest_p0, void* source_p1, NI size_p2);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, dollar___systemZdollars_u3)(NI x_p0);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, rawNewString)(NI space_p0);
N_LIB_PRIVATE N_NOCONV(void, dealloc)(void* p_p0);
N_LIB_PRIVATE N_NIMCALL(void, raiseExceptionEx)(Exception* e_p0, NCSTRING ename_p1, NCSTRING procname_p2, NCSTRING filename_p3, NI line_p4);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
N_LIB_PRIVATE N_NIMCALL(void*, newSeqPayload)(NI cap_p0, NI elemSize_p1, NI elemAlign_p2);
N_LIB_PRIVATE N_NIMCALL(void, setLengthStrV2)(NimStringV2* s_p0, NI newLen_p1);
static N_INLINE(void, nimAddCharV1)(NimStringV2* s_p0, NIM_CHAR c_p1);
N_LIB_PRIVATE N_NIMCALL(void, prepareAdd)(NimStringV2* s_p0, NI addLen_p1);
N_LIB_PRIVATE N_NIMCALL(void, add__stdZenumutils_u71)(tySequence__sM4lkSb7zS6F7OVMvW9cffQ* x_p0, NimStringV2 y_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, eqdup___stdZassertions_u19)(NimStringV2 src_p0);
N_LIB_PRIVATE N_NIMCALL(void, nimTestErrorFlag)(void);
extern TNimTypeV2 NTIv2__rICUfwx5lYXxZ9c1cjKQIxQ_;
static const struct {
  NI cap; NIM_CHAR data[43+1];
} TM__MZPGKsURfItHSi3RvnOT8Q_2 = { 43 | NIM_STRLIT_FLAG, "index out of bounds, the container is empty" };
static const NimStringV2 TM__MZPGKsURfItHSi3RvnOT8Q_3 = {43, (NimStrPayload*)&TM__MZPGKsURfItHSi3RvnOT8Q_2};
static const struct {
  NI cap; NIM_CHAR data[6+1];
} TM__MZPGKsURfItHSi3RvnOT8Q_4 = { 6 | NIM_STRLIT_FLAG, "index " };
static const NimStringV2 TM__MZPGKsURfItHSi3RvnOT8Q_5 = {6, (NimStrPayload*)&TM__MZPGKsURfItHSi3RvnOT8Q_4};
static const struct {
  NI cap; NIM_CHAR data[13+1];
} TM__MZPGKsURfItHSi3RvnOT8Q_6 = { 13 | NIM_STRLIT_FLAG, " not in 0 .. " };
static const NimStringV2 TM__MZPGKsURfItHSi3RvnOT8Q_7 = {13, (NimStrPayload*)&TM__MZPGKsURfItHSi3RvnOT8Q_6};
static const struct {
  NI cap; NIM_CHAR data[0+1];
} TM__MZPGKsURfItHSi3RvnOT8Q_8 = { 0 | NIM_STRLIT_FLAG, "" };
static const NimStringV2 TM__MZPGKsURfItHSi3RvnOT8Q_9 = {0, (NimStrPayload*)&TM__MZPGKsURfItHSi3RvnOT8Q_8};
extern int cmdCount;
extern NCSTRING* cmdLine;
extern NIM_BOOL nimInErrorMode__system_u4238;
N_LIB_PRIVATE N_NIMCALL(NI, paramCount__stdZcmdline_u59)(void) {
	NI result;
	result = ((NI) ((NI32)(cmdCount - ((NI32)1))));
	return result;
}
static N_INLINE(void, nimCopyMem)(void* dest_p0, void* source_p1, NI size_p2) {
	void* T1_;
	T1_ = (void*)0;
	T1_ = memcpy(dest_p0, source_p1, ((size_t) (size_p2)));
}
static N_INLINE(void, copyMem__system_u1740)(void* dest_p0, void* source_p1, NI size_p2) {
	nimCopyMem(dest_p0, source_p1, size_p2);
}
static N_INLINE(void, appendString)(NimStringV2* dest_p0, NimStringV2 src_p1) {
	{
		if (!(((NI)0) < src_p1.len)) goto LA3_;
		copyMem__system_u1740(((void*) ((&(*(*dest_p0).p).data[(*dest_p0).len]))), ((void*) ((&(*src_p1.p).data[((NI)0)]))), ((NI) ((NI)(src_p1.len + ((NI)1)))));
		(*dest_p0).len += src_p1.len;
	}
LA3_: ;
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4238);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, paramStr__stdZcmdline_u53)(NI i_p0) {
	NimStringV2 result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result.len = 0; result.p = NIM_NIL;
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = (((NI32) (i_p0)) < cmdCount);
		if (!(T3_)) goto LA4_;
		T3_ = (((NI)0) <= i_p0);
LA4_: ;
		if (!T3_) goto LA5_;
		result = cstrToNimstr(cmdLine[i_p0]);
	}
	goto LA1_;
LA5_: ;
	{
		NimStringV2 colontmpD_;
		NimStringV2 colontmpD__2;
		tyObject_IndexDefect__rICUfwx5lYXxZ9c1cjKQIxQ* T9_;
		colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
		colontmpD__2.len = 0; colontmpD__2.p = NIM_NIL;
		T9_ = NIM_NIL;
		T9_ = (tyObject_IndexDefect__rICUfwx5lYXxZ9c1cjKQIxQ*) nimNewObj(sizeof(tyObject_IndexDefect__rICUfwx5lYXxZ9c1cjKQIxQ), NIM_ALIGNOF(tyObject_IndexDefect__rICUfwx5lYXxZ9c1cjKQIxQ));
		(*T9_).Sup.Sup.Sup.m_type = (&NTIv2__rICUfwx5lYXxZ9c1cjKQIxQ_);
		(*T9_).Sup.Sup.name = "IndexDefect";
		{
			NimStringV2 blitTmp;
			if (!((NI32)(cmdCount - ((NI32)1)) < ((NI32)0))) goto LA12_;
			colontmpD_ = TM__MZPGKsURfItHSi3RvnOT8Q_3;
			blitTmp = colontmpD_;
			eqwasMoved___stdZassertions_u13((&colontmpD_));
			(*T9_).Sup.Sup.message = blitTmp;
		}
		goto LA10_;
LA12_: ;
		{
			NimStringV2 colontmpD__3;
			NimStringV2 colontmpD__4;
			NimStringV2 T15_;
			NimStringV2 blitTmp_2;
			colontmpD__3.len = 0; colontmpD__3.p = NIM_NIL;
			colontmpD__4.len = 0; colontmpD__4.p = NIM_NIL;
			T15_.len = 0; T15_.p = NIM_NIL;
			colontmpD__3 = dollar___systemZdollars_u3(i_p0);
			if (NIM_UNLIKELY(*nimErr_)) goto LA8_;
			colontmpD__4 = dollar___systemZdollars_u3(((NI) ((NI32)(cmdCount - ((NI32)1)))));
			if (NIM_UNLIKELY(*nimErr_)) goto LA8_;
			T15_ = rawNewString(colontmpD__3.len + colontmpD__4.len + 19);
appendString((&T15_), TM__MZPGKsURfItHSi3RvnOT8Q_5);
appendString((&T15_), colontmpD__3);
appendString((&T15_), TM__MZPGKsURfItHSi3RvnOT8Q_7);
appendString((&T15_), colontmpD__4);
			colontmpD__2 = T15_;
			if (colontmpD__4.p && !(colontmpD__4.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(colontmpD__4.p);
}
			if (colontmpD__3.p && !(colontmpD__3.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(colontmpD__3.p);
}
			blitTmp_2 = colontmpD__2;
			eqwasMoved___stdZassertions_u13((&colontmpD__2));
			(*T9_).Sup.Sup.message = blitTmp_2;
		}
LA10_: ;
		(*T9_).Sup.Sup.parent = ((Exception*) NIM_NIL);
		raiseExceptionEx((Exception*)T9_, "IndexDefect", "paramStr", "cmdline.nim", 272);
goto LA8_;
		{
			LA8_:;
		}
		{
			if (colontmpD__2.p && !(colontmpD__2.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(colontmpD__2.p);
}
			if (colontmpD_.p && !(colontmpD_.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(colontmpD_.p);
}
		}
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
LA1_: ;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(void, nimAddCharV1)(NimStringV2* s_p0, NIM_CHAR c_p1) {
	prepareAdd(s_p0, ((NI)1));
	(*(*s_p0).p).data[(*s_p0).len] = c_p1;
	(*s_p0).len += ((NI)1);
	(*(*s_p0).p).data[(*s_p0).len] = 0;
}
N_LIB_PRIVATE N_NIMCALL(tySequence__sM4lkSb7zS6F7OVMvW9cffQ, nosparseCmdLine)(NimStringV2 c_p0) {
	tySequence__sM4lkSb7zS6F7OVMvW9cffQ result;
	NimStringV2 a;
	NI i;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result.len = 0; result.p = NIM_NIL;
	a.len = 0; a.p = NIM_NIL;
	result.len = 0; result.p = (tySequence__sM4lkSb7zS6F7OVMvW9cffQ_Content*) newSeqPayload(0, sizeof(NimStringV2), NIM_ALIGNOF(NimStringV2));
	i = ((NI)0);
	a = TM__MZPGKsURfItHSi3RvnOT8Q_9;
	{
		while (1) {
			NimStringV2 colontmpD_;
			colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
			setLengthStrV2((&a), ((NI)0));
			{
				while (1) {
					NIM_BOOL T6_;
					T6_ = (NIM_BOOL)0;
					T6_ = (i < c_p0.len);
					if (!(T6_)) goto LA7_;
					T6_ = (((NU8)(c_p0.p->data[i])) == ((NU8)(32)) || ((NU8)(c_p0.p->data[i])) == ((NU8)(9)) || ((NU8)(c_p0.p->data[i])) == ((NU8)(10)) || ((NU8)(c_p0.p->data[i])) == ((NU8)(13)));
LA7_: ;
					if (!T6_) goto LA5;
					i += ((NI)1);
				} LA5: ;
			}
			{
				if (!(c_p0.len <= i)) goto LA10_;
				goto LA2;
			}
LA10_: ;
			switch (((NU8)(c_p0.p->data[i]))) {
			case 39:
			case 34:
			{
				NIM_CHAR delim;
				delim = c_p0.p->data[i];
				i += ((NI)1);
				{
					while (1) {
						NIM_BOOL T15_;
						T15_ = (NIM_BOOL)0;
						T15_ = (i < c_p0.len);
						if (!(T15_)) goto LA16_;
						T15_ = !(((NU8)(c_p0.p->data[i]) == (NU8)(delim)));
LA16_: ;
						if (!T15_) goto LA14;
						nimAddCharV1((&a), c_p0.p->data[i]);
						i += ((NI)1);
					} LA14: ;
				}
				{
					if (!(i < c_p0.len)) goto LA19_;
					i += ((NI)1);
				}
LA19_: ;
			}
			break;
			default:
			{
				{
					while (1) {
						NIM_BOOL T24_;
						T24_ = (NIM_BOOL)0;
						T24_ = (i < c_p0.len);
						if (!(T24_)) goto LA25_;
						T24_ = ((NU8)(32) < (NU8)(c_p0.p->data[i]));
LA25_: ;
						if (!T24_) goto LA23;
						nimAddCharV1((&a), c_p0.p->data[i]);
						i += ((NI)1);
					} LA23: ;
				}
			}
			break;
			}
			colontmpD_ = eqdup___stdZassertions_u19(a);
			add__stdZenumutils_u71((&result), colontmpD_);
		}
	} LA2: ;
	{
		LA1_:;
	}
	{
		if (a.p && !(a.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(a.p);
}
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
