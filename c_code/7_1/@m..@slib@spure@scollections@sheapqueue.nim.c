/* Generated by Nim Compiler v2.0.8 */
#define NIM_INTBITS 32

#include "nimbase.h"
#include <string.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg;
typedef struct tySequence__WsWg9boMtNW6O9bDQUM2NlpA tySequence__WsWg9boMtNW6O9bDQUM2NlpA;
typedef struct tySequence__WsWg9boMtNW6O9bDQUM2NlpA_Content tySequence__WsWg9boMtNW6O9bDQUM2NlpA_Content;
typedef struct tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg;
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ;
typedef struct tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA;
struct tySequence__WsWg9boMtNW6O9bDQUM2NlpA {
  NI len; tySequence__WsWg9boMtNW6O9bDQUM2NlpA_Content* p;
};
struct tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg {
	tySequence__WsWg9boMtNW6O9bDQUM2NlpA data;
};
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
struct tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg {
	NI dist;
	NI depth;
	NimStringV2 msg;
	tyObject_TSym__76PJNQ0heVJuHevrVzIaNQ* sym;
};
struct tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA {
	NI rc;
	NI rootIdx;
};
struct tySequence__WsWg9boMtNW6O9bDQUM2NlpA_Content { NI cap; tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg data[SEQ_DECL_SIZE]; };
static N_INLINE(void, nimZeroMem)(void* p_p0, NI size_p1);
static N_INLINE(void, nimSetMem__systemZmemory_u7)(void* a_p0, int v_p1, NI size_p2);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
N_LIB_PRIVATE N_NIMCALL(void, add__lookups_u3465)(tySequence__WsWg9boMtNW6O9bDQUM2NlpA* x_p0, tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg y_p1);
N_LIB_PRIVATE N_NIMCALL(void, siftup__lookups_u3550)(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg* heap_p0, NI startpos_p1, NI p_p2);
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___lookups_u3710)(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* dest_p0, tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg src_p1);
static N_INLINE(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg*, X5BX5D___lookups_u3610)(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg* heap_p0, NI i_p1);
static N_INLINE(NIM_BOOL, heapCmp__lookups_u3699)(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg x_p0, tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg y_p1);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, lt___lookups_u3415)(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg a_p0, tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg b_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqsink___lookups_u3716)(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* dest_p0, tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg src_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___lookups_u3704)(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___lookups_u3707)(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* dest_p0);
static N_INLINE(NI, len__lookups_u3543)(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg heap_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___lookups_u4514)(tySequence__WsWg9boMtNW6O9bDQUM2NlpA dest_p0);
N_LIB_PRIVATE N_NOCONV(void, dealloc)(void* p_p0);
static N_INLINE(NIM_BOOL, nimDecRefIsLastDyn)(void* p_p0);
static N_INLINE(NI, minuspercent___system_u810)(NI x_p0, NI y_p1);
N_LIB_PRIVATE N_NIMCALL(void, unregisterCycle__system_u3007)(tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA* s_p0);
N_LIB_PRIVATE N_NIMCALL(void, nimDestroyAndDispose)(void* p_p0);
N_LIB_PRIVATE N_NIMCALL(void, alignedDealloc)(void* p_p0, NI align_p1);
static N_INLINE(void, pop__lookups_u3943)(tySequence__WsWg9boMtNW6O9bDQUM2NlpA* s_p0, tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* Result);
N_LIB_PRIVATE N_NIMCALL(void, shrink__lookups_u3958)(tySequence__WsWg9boMtNW6O9bDQUM2NlpA* x_p0, NI newLen_p1);
N_LIB_PRIVATE N_NIMCALL(void, siftdownToBottom__lookups_u4178)(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg* heap_p0, NI p_p1);
N_LIB_PRIVATE N_NIMCALL(void, nimTestErrorFlag)(void);
extern NIM_BOOL nimInErrorMode__system_u4215;
static N_INLINE(void, nimSetMem__systemZmemory_u7)(void* a_p0, int v_p1, NI size_p2) {
	void* T1_;
	T1_ = (void*)0;
	T1_ = memset(a_p0, v_p1, ((size_t) (size_p2)));
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4215);
	return result;
}
static N_INLINE(void, nimZeroMem)(void* p_p0, NI size_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	nimSetMem__systemZmemory_u7(p_p0, ((int)0), size_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg, initHeapQueue__lookups_u3448)(void) {
	tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg result;
	nimZeroMem((void*)(&result), sizeof(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg));
	return result;
}
static N_INLINE(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg*, X5BX5D___lookups_u3610)(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg* heap_p0, NI i_p1) {
	tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* result;
	result = (&(*heap_p0).data.p->data[i_p1]);
	return result;
}
static N_INLINE(NIM_BOOL, heapCmp__lookups_u3699)(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg x_p0, tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg y_p1) {
	NIM_BOOL result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = lt___lookups_u3415(x_p0, y_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, siftup__lookups_u3550)(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg* heap_p0, NI startpos_p1, NI p_p2) {
	tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg newitem;
	NI pos;
	tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* T2_;
NIM_BOOL oldNimErrFin5_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	nimZeroMem((void*)(&newitem), sizeof(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg));
	pos = p_p2;
	T2_ = (tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg*)0;
	T2_ = X5BX5D___lookups_u3610((&(*heap_p0)), ((NI) (pos)));
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	eqcopy___lookups_u3710((&newitem), (*T2_));
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		while (1) {
			tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg parent;
			NI parentpos;
			tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* T6_;
			if (!(startpos_p1 < pos)) goto LA4;
			nimZeroMem((void*)(&parent), sizeof(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg));
			parentpos = (NI)((NI32)((NI)(pos - ((NI)1))) >> (NU32)(((NI)1)));
			T6_ = (tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg*)0;
			T6_ = X5BX5D___lookups_u3610((&(*heap_p0)), ((NI) (parentpos)));
			if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
			eqcopy___lookups_u3710((&parent), (*T6_));
			if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
			{
				NIM_BOOL T9_;
				T9_ = (NIM_BOOL)0;
				T9_ = heapCmp__lookups_u3699(newitem, parent);
				if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
				if (!T9_) goto LA10_;
				eqsink___lookups_u3716((&(*heap_p0).data.p->data[pos]), parent);
				if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
				eqwasMoved___lookups_u3704((&parent));
				if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
				pos = parentpos;
			}
			goto LA7_;
LA10_: ;
			{
				eqdestroy___lookups_u3707((&parent));
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
				goto LA3;
			}
LA7_: ;
			{
				LA5_:;
			}
			{
				oldNimErrFin5_ = *nimErr_; *nimErr_ = NIM_FALSE;
				eqdestroy___lookups_u3707((&parent));
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
				*nimErr_ = oldNimErrFin5_;
			}
			if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
		} LA4: ;
	} LA3: ;
	eqsink___lookups_u3716((&(*heap_p0).data.p->data[pos]), newitem);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		LA1_:;
	}
	{
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
static N_INLINE(NI, len__lookups_u3543)(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg heap_p0) {
	NI result;
	NI T1_;
	T1_ = heap_p0.data.len;
	result = T1_;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, push__lookups_u3461)(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg* heap_p0, tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg item_p1) {
	tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg blitTmp;
	NI T1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	nimZeroMem((void*)(&blitTmp), sizeof(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg));
	blitTmp.dist = item_p1.dist;
	blitTmp.depth = item_p1.depth;
	blitTmp.msg = item_p1.msg;
	blitTmp.sym = item_p1.sym;
	add__lookups_u3465((&(*heap_p0).data), blitTmp);
	T1_ = (NI)0;
	T1_ = len__lookups_u3543((*heap_p0));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	siftup__lookups_u3550(heap_p0, ((NI)0), (NI)(T1_ - ((NI)1)));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
static N_INLINE(NI, minuspercent___system_u810)(NI x_p0, NI y_p1) {
	NI result;
	result = ((NI) ((NU)((NU32)(((NU) (x_p0))) - (NU32)(((NU) (y_p1))))));
	return result;
}
static N_INLINE(NIM_BOOL, nimDecRefIsLastDyn)(void* p_p0) {
	NIM_BOOL result;
	result = (NIM_BOOL)0;
	{
		tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA* cell;
		NI T5_;
		if (!!((p_p0 == NIM_NIL))) goto LA3_;
		T5_ = (NI)0;
		T5_ = minuspercent___system_u810(((NI) (ptrdiff_t) (p_p0)), ((NI)8));
		cell = ((tyObject_RefHeader__wvKSfRQyMm9an0DxnbhM6TA*) (T5_));
		{
			if (!((NI)((*cell).rc & ((NI)-16)) == ((NI)0))) goto LA8_;
			result = NIM_TRUE;
		}
		goto LA6_;
LA8_: ;
		{
			(*cell).rc -= ((NI)16);
		}
LA6_: ;
		{
			if (!result) goto LA13_;
			{
				if (!(((NI)0) < (*cell).rootIdx)) goto LA17_;
				unregisterCycle__system_u3007(cell);
			}
LA17_: ;
		}
LA13_: ;
	}
LA3_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___lookups_u4514)(tySequence__WsWg9boMtNW6O9bDQUM2NlpA dest_p0) {
	NI colontmp_;
	colontmp_ = ((NI)0);
	{
		while (1) {
			NI T3_;
			T3_ = dest_p0.len;
			if (!(colontmp_ < T3_)) goto LA2;
			if (dest_p0.p->data[colontmp_].msg.p && !(dest_p0.p->data[colontmp_].msg.p->cap & NIM_STRLIT_FLAG)) {
 dealloc(dest_p0.p->data[colontmp_].msg.p);
}
			{
				NIM_BOOL T6_;
				T6_ = (NIM_BOOL)0;
				T6_ = nimDecRefIsLastDyn(dest_p0.p->data[colontmp_].sym);
				if (!T6_) goto LA7_;
				nimDestroyAndDispose(dest_p0.p->data[colontmp_].sym);
			}
LA7_: ;
			colontmp_ += ((NI)1);
		} LA2: ;
	}
	if (dest_p0.p && !(dest_p0.p->cap & NIM_STRLIT_FLAG)) {
 alignedDealloc(dest_p0.p, NIM_ALIGNOF(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg));
}
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___lookups_u4496)(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg* dest_p0) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	eqdestroy___lookups_u4514((*dest_p0).data);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
static N_INLINE(void, pop__lookups_u3943)(tySequence__WsWg9boMtNW6O9bDQUM2NlpA* s_p0, tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* Result) {
	NI L;
	NI T1_;
	T1_ = (*s_p0).len;
	L = (NI)(T1_ - ((NI)1));
	(*Result).dist = (*s_p0).p->data[L].dist;
	(*Result).depth = (*s_p0).p->data[L].depth;
	(*Result).msg = (*s_p0).p->data[L].msg;
	(*Result).sym = (*s_p0).p->data[L].sym;
	eqwasMoved___lookups_u3704((&(*s_p0).p->data[L]));
	shrink__lookups_u3958(s_p0, ((NI) (L)));
}
N_LIB_PRIVATE N_NIMCALL(void, siftdownToBottom__lookups_u4178)(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg* heap_p0, NI p_p1) {
	tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg newitem;
	NI endpos;
	NI pos;
	NI startpos;
	tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* T1_;
	NI childpos;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	nimZeroMem((void*)(&newitem), sizeof(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg));
	endpos = len__lookups_u3543((*heap_p0));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	pos = p_p1;
	startpos = pos;
	T1_ = (tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg*)0;
	T1_ = X5BX5D___lookups_u3610((&(*heap_p0)), ((NI) (pos)));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	eqcopy___lookups_u3710((&newitem), (*T1_));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	childpos = (NI)((NI)(((NI)2) * pos) + ((NI)1));
	{
		while (1) {
			NI rightpos;
			tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* T13_;
			if (!(childpos < endpos)) goto LA3;
			rightpos = (NI)(childpos + ((NI)1));
			{
				NIM_BOOL T6_;
				tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* T8_;
				tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* T9_;
				NIM_BOOL T10_;
				T6_ = (NIM_BOOL)0;
				T6_ = (rightpos < endpos);
				if (!(T6_)) goto LA7_;
				T8_ = (tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg*)0;
				T8_ = X5BX5D___lookups_u3610((&(*heap_p0)), ((NI) (childpos)));
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				T9_ = (tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg*)0;
				T9_ = X5BX5D___lookups_u3610((&(*heap_p0)), ((NI) (rightpos)));
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				T10_ = (NIM_BOOL)0;
				T10_ = heapCmp__lookups_u3699((*T8_), (*T9_));
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				T6_ = !(T10_);
LA7_: ;
				if (!T6_) goto LA11_;
				childpos = rightpos;
			}
LA11_: ;
			T13_ = (tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg*)0;
			T13_ = X5BX5D___lookups_u3610((&(*heap_p0)), ((NI) (childpos)));
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			eqcopy___lookups_u3710((&(*heap_p0).data.p->data[pos]), (*T13_));
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			pos = childpos;
			childpos = (NI)((NI)(((NI)2) * pos) + ((NI)1));
		} LA3: ;
	}
	eqsink___lookups_u3716((&(*heap_p0).data.p->data[pos]), newitem);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	siftup__lookups_u3550(heap_p0, startpos, pos);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, pop__lookups_u3939)(tyObject_HeapQueue__H9a2QmiRAo9alGBSFptC4kAg* heap_p0, tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* Result) {
	tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg lastelt;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	nimZeroMem((void*)Result, sizeof(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg));
	nimZeroMem((void*)(&lastelt), sizeof(tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg));
	pop__lookups_u3943((&(*heap_p0).data), (&lastelt));
	{
		NI T3_;
		tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg* T6_;
		T3_ = (NI)0;
		T3_ = len__lookups_u3543((*heap_p0));
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		if (!(((NI)0) < T3_)) goto LA4_;
		T6_ = (tyObject_SpellCandidate__EUxhDxvBWY1TKHGby2enbg*)0;
		T6_ = X5BX5D___lookups_u3610((&(*heap_p0)), ((NI)0));
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqcopy___lookups_u3710(Result, (*T6_));
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqsink___lookups_u3716((&(*heap_p0).data.p->data[((NI)0)]), lastelt);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		siftdownToBottom__lookups_u4178(heap_p0, ((NI)0));
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
	goto LA1_;
LA4_: ;
	{
		(*Result).dist = lastelt.dist;
		(*Result).depth = lastelt.depth;
		(*Result).msg = lastelt.msg;
		(*Result).sym = lastelt.sym;
	}
LA1_: ;
	}BeforeRet_: ;
}
