#!/usr/bin/make -f

export DH_VERBOSE=1
include /usr/share/dpkg/default.mk

export XDG_CACHE_HOME=$(CURDIR)/.cache
export XDG_CONFIG_HOME=$(CURDIR)/.config
#export XDG_DATA_HOME=$(CURDIR)/.data

platform_tr = $(shell { grep '^$(1)\b' debian/platforms.table || echo '	unsupported:$(1)'; } | cut -f2)

NIMFLAGS = $(addprefix --passC:,$(CPPFLAGS)) $(addprefix --passC:,$(CFLAGS)) $(addprefix --passL:,$(LDFLAGS))
ifeq ($(DEB_HOST_ARCH_OS),kfreebsd)
	# work around https://github.com/nim-lang/Nim/issues/3165
	NIMFLAGS += --parallelBuild:1
endif

%:
	dh $@

override_dh_auto_build:
	echo "running make to build ./bin/nim from csources"
	make -j
	echo "building ./koch"
	./bin/nim c -d:release $(NIMFLAGS) koch
	# Build until convergence; we must do this here, not dh_auto_test, to pick up
	# any changes in debian/patches. see also https://github.com/nim-lang/csources/issues/12
	echo "running koch boot to build ./bin/nim from .nim sources"
	./koch boot -d:release $(NIMFLAGS)
	echo "running koch web to generate docs into doc/html"
	./koch docs --docCmd:skip
	echo "running koch tools to build nimble, nimsuggest, nimgrep"
	cp -rp pkgs/atlas dist/
	mkdir dist/atlas/dist
	cp -rp dist/nimble/dist/sat dist/atlas/dist
	./koch tools
	echo "generating manpages"
	help2man --name="Nim Language Compiler" -s1 -N --version-string="$(DEB_VERSION)" -i debian/nim.h2m -o debian/nim.1 ./bin/nim
	help2man --name="Nimsuggest"            -s1 -N --version-string="$(DEB_VERSION)" -i debian/nim.h2m -o debian/nimsuggest.1 ./bin/nimsuggest
	help2man --name="Nimgrep"               -s1 -N --version-string="$(DEB_VERSION)" -i debian/nim.h2m -o debian/nimgrep.1 ./bin/nimgrep
	help2man --name="Nimpretty"             -s1 -N --version-string="$(DEB_VERSION)" -i debian/nim.h2m -o debian/nimpretty.1 ./bin/nimpretty
	help2man --name="Nim Package Installer" -s1 -N --version-string="$(DEB_VERSION)" -i debian/nim.h2m -o debian/nimble.1 ./bin/nimble
	echo "running koch distrohelper to generate install.sh"
	./koch distrohelper

override_dh_auto_install:
	echo "running install.sh to populate debian/tmp"
	./install.sh debian/tmp
	# remove the compiler sources directory
	# https://github.com/nim-lang/Nim/issues/4866#issuecomment-271194805
	rm debian/tmp/nim/compiler -rf
	for fn in nimble nimsuggest nimgrep nimpretty nim-gdb; do install ./bin/$$fn debian/tmp/nim/bin/; done
	# Install tools
	install -D -m 644 tools/dochack/dochack.nim     debian/tmp/nim/tools/dochack/dochack.nim
	install -D -m 644 tools/dochack/fuzzysearch.nim debian/tmp/nim/tools/dochack/fuzzysearch.nim
	install -D -m 644 tools/dochack/dochack.js      debian/tmp/nim/tools/dochack/dochack.js
	install -D -m 644 tools/debug/nim-gdb.py        debian/tmp/nim/tools/debug/nim-gdb.py
	# Install docs. There is no version number in the path.
	install -d debian/html
	install -D doc/html/*.html doc/html/*.css doc/html/*.js debian/html
	find debian/html -name '*.idx' -delete
	ln -sf manual.html debian/html/index.html
	# install bash/zsh completion scripts
	install -D -m 644 tools/nim.bash-completion          debian/nim/usr/share/bash-completion/completions/nim
	install -D -m 644 dist/nimble/nimble.bash-completion debian/nim/usr/share/bash-completion/completions/nimble
	install -D -m 644 tools/nim.zsh-completion           debian/nim/usr/share/zsh/vendor-completions/_nim
	install -D -m 644 dist/nimble/nimble.zsh-completion  debian/nim/usr/share/zsh/vendor-completions/_nimble
	rm -f debian/tmp/nim/compiler.nimble debian/tmp/nim/doc/advopt.txt debian/tmp/nim/nim.nimble
	rm -f debian/tmp/nim/doc/basicopt.txt debian/tmp/nim/doc/html/overview.html
	install -D -m 644 doc/nimdoc.css  debian/nim/usr/lib/nim/doc/nimdoc.css
	rm debian/tmp/nim/doc/nimdoc.css

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	#PATH=./bin:$$PATH ./koch test
	if [ -f compiler/nim2 ]; then cmp ./bin/nim compiler/nim2; else cmp ./bin/nim compiler/nim1; fi
	# TODO: do this with autopkgtest instead; it requires libsqlite3-0 and nodejs
	#PATH=./bin:$$PATH ./koch test
endif

override_dh_auto_clean:
	rm -f ./koch tools/nimweb tools/niminst/niminst compiler/nim compiler/nim0 compiler/nim1 compiler/nim2
	rm -f lib/pure/strutils_examples.nim
	rm -f bin/atlas bin/nim bin/nim_dbg bin/nimble bin/nimgrep bin/nimpretty bin/nimsuggest bin/testament
	rm -rf rnimcache tools/dochack/dochack.js  doc/html/ .cache
	rm -rf web/upload/
	rm -rf dist/nimble0 dist/nimble/src/nimble
	# TODO: use this instead when https://github.com/nim-lang/Nim/issues/2127 is fixed
	#PATH=./bin:$$PATH ./koch clean
	find . -name nimcache \
		-prune -execdir rm -rf nimcache \;
	find . -name '*.o' -not -name 'koch_icon.o' -and -not -name 'nim_icon.o' \
		-delete -print
	find doc -name '*.idx' -delete -print
	find doc -name '*.html' -delete -print
	find lib tests -type f -executable -and -not -name '*.nim' \
		-delete -print
	rm -f tests/dll/libnimrtl.dylib
	rm -f testament.db testresults.*

override_dh_dwz:
	# dwz cannot handle all the binaries together
	# dh_dwz --no-dwz-multifile
	# dwz errors out when handling nim
	true

override_dh_installdocs:
	dh_installdocs -Xcopying.txt

override_dh_clean:
	dh_clean
	rm -f debian/nim.1 debian/nimble.1 debian/nimgrep.1 debian/nimsuggest.1 debian/nimpretty.1
	rm -rf debian/html
